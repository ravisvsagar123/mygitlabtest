import React, { Component } from "react";
import Select from "react-select";

import accbg from '../ui-assets/images/Rectangle.svg'
import succ_img from '../ui-assets/images/SUCCESS.svg'
import countries from "../ui-config/countries.json"
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import CloseIcon from "@material-ui/icons/Close";

import {
    KeyboardDatePicker,
    MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import { Grid, Typography, Button, IconButton } from '@material-ui/core'
import CustomTextField from "../ui-atoms/components/CustomTextField"

import DateFnsUtils from '@date-io/date-fns';
const select_style = {
    control: base => ({
        ...base,
        border: 0,
        // This line disable the blue border
        boxShadow: "none",
        fontSize: '14px',
        fontFamily: 'OpenSans-Regular',
        colour: '#999999'
    })
};



const styles = {
    mainback: {
        height: '768px',
        width: 'auto',
        backgroundImage: `url(${accbg})`,

        backgroundPosition: 'center',
        margin: '0px 0px',
        backgroundSize: "cover",
    },
    mainheader: {
        color: "#ffffff",
        fontFamily: "OpenSans-Bold",
        fontSize: "24px",
    },
    mainheadersub: {
        color: "#ffffff",
        fontFamily: "OpenSans-Regular",
        fontSize: "18px",
    },
    mainbacksub: {
        width: '830px',
        background: '#fff',
        borderRadius: '12px',
        justifyContent: 'center',
        margin: '10px 75px 1px 268px'
    },
    demohead: {
        color: "#333333",
        fontFamily: "OpenSans-Regular",
        fontSize: "18px",
    },
    line: {
        border: '#dddddd 1px solid',
    },
    field: {
        marginLeft: "2px",
        marginRight: "2px",
        height: '34px',
        width: '300px',
    },
    field1: {
        marginLeft: "2px",
        marginRight: "2px",
        height: '45px',
        width: '300px',
    },
    backbutton: {
        color: "#2f3253",
        fontFamily: "OpenSans-Regular",
        fontSize: "14px",
        cursor: "pointer",
    },
    errsty: {
        color: "red",
        fontFamily: "OpenSans-Regular",
        fontSize: "10px",
    }
};
const countriesData = countries.map(data => {
    return { label: data.country, value: data.countryISO2 };
});

const acctype = [
    { value: 'STANDARD', label: 'STANDARD' },
    { value: 'ELITE', label: 'ELITE' },
    { value: 'PREMIUM', label: 'PREMIUM' },
]


const cltype = [
    { value: 'Individual_Account', label: 'Individual Account' },
    { value: 'Common_Account', label: 'Common Account' },
    { value: 'Corporate_Account', label: ' Corporate Account' },
]

const currtype = [
    { value: 'EUR', label: 'EUR' },
    { value: 'USD', label: 'USD' },

]


export class OpenDemo1 extends Component {

    constructor(props) {
        super(props)

        this.state = {

            open: false,
            isfname: false,
            fname: '',
            islname: false,
            lname: '',
            isem: false,
            emailid: '',
            ismob: false,
            mobno: '',
            iscname: false,
            cname: '',
            iscutype: false,
            cuname: '',
            isacctype: false,
            acctye: '',
            iscltype: false,
            clname: '',
            password1: '',
            password2: '',
            ispassword1: false,
            ispassword2: false,
            openDialog: false,
            selectedDate: new Date()

        }
    }
    HandelVailad() {
        var isok = '0';
        /* var fname = this.state.fname;
          var lname = this.state.lname;
          var emailid = this.state.emailid;
          var mobno = this.state.mobno;
  
          var cname = this.state.cname;
  
          var cuname = this.state.cuname;
  
          var acctye = this.state.acctye;
          var clname = this.state.clname;
          var password1 = this.state.password1;
          var password2 = this.state.password2;
  
  
  
          if (!fname.length) {
              this.setState({ isfname: true })
              isok = '1';
          }
          if (!lname.length) {
              this.setState({ islname: true })
              isok = '1';
          }
          if (!emailid.length) {
              this.setState({ isem: true })
              isok = '1';
          }
          if (!mobno.length) {
              this.setState({ ismob: true })
              isok = '1';
          }
          if (!cname.length) {
              this.setState({ iscname: true })
              isok = '1';
          }
          if (!cuname.length) {
              this.setState({ iscutype: true })
              isok = '1';
          }
          if (!acctye.length) {
              this.setState({ isacctype: true })
              isok = '1';
          }
          if (!clname.length) {
              this.setState({ iscltype: true })
              isok = '1';
          }
          if (!password1.length) {
              this.setState({ ispassword1: true })
              isok = '1';
          }
          if (!password2.length) {
              this.setState({ ispassword2: true })
              isok = '1';
          }*/
        if (isok === '0') {
            this.setState({ openDialog: true });
            // this.props.history.push('/dashbord');
        }
    }
    handleDateChange(date) {

        this.setState({ selectedDate: date })
    }



    render() {

        const { selectedDate,
            isfname, fname,
            islname, lname,
            isem, emailid,
            ismob, mobno,
            iscname,
            iscutype,
            isacctype,
            iscltype,
            password1, ispassword1,
            password2, ispassword2,

            openDialog
        } = this.state


        return (

            <Grid style={styles.mainback}>
                <Grid style={{ justifyContent: 'center', display: 'flex' }}>
                    <Typography style={styles.mainheader}>Registration. Use form below</Typography>
                </Grid>
                <Grid style={{ justifyContent: 'center', display: 'flex' }}>
                    <Typography style={styles.mainheadersub}>Please fill in your details to create a trading account.</Typography>
                </Grid>

                <Grid direction='row' style={styles.mainbacksub}>
                    <Grid style={{ justifyContent: 'center', display: 'flex', marginBottom: '30px' }} >
                        <Typography style={styles.demohead}>Create Demo Account</Typography>
                    </Grid>
                    <Grid style={styles.line}></Grid>
                    <Grid container style={{ paddingLeft: '89px', paddingTop: '31px' }}>
                        <Grid container item xs={6}>
                            <Grid>
                                <CustomTextField
                                    autoFocus={true}
                                    style={styles.field}
                                    placeholder="First Name*"

                                    value={fname}
                                    handleChange={e =>
                                        this.setState({ fname: e.target.value, isfname: false })

                                    }

                                />
                                {isfname && <span style={styles.errsty}>Enter the First Name</span>}
                            </Grid>
                            <Grid style={{ paddingTop: '16px' }}>
                                <CustomTextField
                                    autoFocus={true}
                                    style={styles.field}
                                    placeholder="Last Name*"
                                    value={lname}

                                    handleChange={e =>
                                        this.setState({ lname: e.target.value, islname: false })

                                    }
                                />
                                {islname && <span style={styles.errsty}>Enter the Last Name</span>}
                            </Grid>
                            <Grid container style={{ paddingTop: '16px' }}>


                                <Grid item xs={4} style={{ fontSize: '12px', colour: '#666666', fontFamily: 'OpenSans-Regular', display: 'flex', alignItems: 'center', justifyContent: 'left' }}>Date Of Birth*</Grid>
                                <Grid item xs={6}>
                                    <MuiPickersUtilsProvider

                                        style={{ height: '20px' }} utils={DateFnsUtils}>
                                        <KeyboardDatePicker

                                            value={selectedDate}
                                            onChange={date => { this.handleDateChange(date) }}
                                            format="yyyy/MM/dd"

                                            inputVariant="standard"
                                            PopoverProps={{
                                                style: { backgroundColor: 'red' }
                                            }}
                                            KeyboardButtonProps={{
                                                style: { marginLeft: -24 }
                                            }}

                                        />
                                    </MuiPickersUtilsProvider>

                                </Grid>
                            </Grid>
                            <Grid style={{ paddingTop: '16px' }}>

                                <CustomTextField
                                    autoFocus={true}
                                    style={styles.field}
                                    placeholder="Email*"

                                    value={emailid}

                                    handleChange={e =>
                                        this.setState({ emailid: e.target.value, isem: false })

                                    }

                                />
                                {isem && <span style={styles.errsty}>Enter the Email id</span>}
                            </Grid>
                            <Grid style={{ paddingTop: '16px' }}>

                                <CustomTextField
                                    autoFocus={true}
                                    style={styles.field}
                                    placeholder="Mobile*"
                                    value={mobno}

                                    handleChange={e =>
                                        this.setState({ mobno: e.target.value, ismob: false })

                                    }



                                />
                                {ismob && <span style={styles.errsty}>Enter the Mobile Number</span>}
                            </Grid>
                            <Grid style={{ paddingTop: '16px' }}>

                                <CustomTextField
                                    autoFocus={true}
                                    style={styles.field}
                                    placeholder="Address"



                                />
                            </Grid>
                        </Grid>


                        <Grid container item xs={6}>
                            <Grid style={{ width: '300px', height: '45px' }}>


                                <Select ref={(input) => this.menu = input} styles={select_style} options={countriesData} placeholder="Country"

                                    onChange={e => {
                                        this.setState({ cname: e.value, iscname: false });
                                    }}
                                />
                                {iscname && <span style={styles.errsty}>Selec the Country Name</span>}
                            </Grid>

                            <Grid style={{ width: '300px', paddingTop: '16px' }}>


                                <Select styles={select_style} options={currtype} placeholder="Currency choice"

                                    onChange={e =>
                                        this.setState({ cuname: e.value, iscutype: false })

                                    }

                                />
                            </Grid>
                            {iscutype && <span style={styles.errsty}>Selec the Currency</span>}
                            <Grid style={{ width: '300px', paddingTop: '16px' }}>


                                <Select styles={select_style} options={acctype} placeholder="Account type"

                                    onChange={e =>
                                        this.setState({ acctye: e.value, isacctype: false })

                                    }

                                />
                            </Grid>
                            {isacctype && <span style={styles.errsty}>Selec the Account Type</span>}
                            <Grid style={{ width: '300px', paddingTop: '16px' }}>


                                <Select styles={select_style} options={cltype} placeholder="Client type"


                                    onChange={e =>
                                        this.setState({ clname: e.value, iscltype: false })

                                    }


                                />
                                {iscltype && <span style={styles.errsty}>Selec the Client Type</span>}
                            </Grid>
                            <Grid style={{ width: '300px', paddingTop: '16px' }}>
                                <CustomTextField
                                    autoFocus={true}
                                    style={styles.field}
                                    value={password1}
                                    placeholder="Password*"
                                    fullWidth={true}
                                    type="Password"
                                    hasEndAdornment={true}
                                    handleChange={e =>
                                        this.setState({ password1: e.target.value, ispassword1: false })

                                    }
                                />
                                {ispassword1 && <span style={styles.errsty}>Enter the Password</span>}
                            </Grid>

                            <Grid style={{ width: '300px', paddingTop: '16px' }}>
                                <CustomTextField
                                    autoFocus={true}
                                    style={styles.field}
                                    placeholder="Confirmation Password8"
                                    fullWidth={true}
                                    value={password2}
                                    type="Password"
                                    hasEndAdornment={true}
                                    handleChange={e =>
                                        this.setState({ password2: e.target.value, ispassword2: false })

                                    }
                                />
                                {ispassword2 && <span style={styles.errsty}>Enter the Password</span>}
                            </Grid>

                        </Grid>
                    </Grid>
                    <Grid container xs={12} style={{ paddingTop: '50px', display: 'flex' }}>
                        <Grid item xs={3} style={{ paddingLeft: '50px' }}>
                            <Typography style={styles.backbutton}

                                onClick={() =>
                                    this.props.history.push("/signup")
                                }
                            >&lt; BACK</Typography>

                        </Grid>
                        <Grid item xs={3}></Grid>
                        <Grid item xs={3}></Grid>
                        <Grid item xs={3}>

                            <Button


                                backgroundColor="transparent"
                                variant="contained"

                                style={{
                                    color: "#2f3253",
                                    fontFamily: "OpenSans-Bold",
                                    borderRadius: "4px",
                                    backgroundColor: "#f4d553",
                                    fontSize: "14px",
                                    lineHeight: "19px",
                                    textTransform: "none",
                                    padding: "12px 16px",
                                    marginBottom: "5%"

                                }}
                                onClick={() => { this.HandelVailad() }}
                            >

                                START TRADING
                            </Button>

                        </Grid>
                    </Grid>
                </Grid>
                <Dialog
                    onClose={() => this.closeContactModal()}
                    open={openDialog}

                    fullWidth
                    BackdropProps={{
                        style: {
                            backgroundColor: "#rgba(0, 0, 0, 0.16)",
                            backdropFilter: "blur(30px)"
                        }
                    }}
                    PaperProps={{
                        style: {
                            background: "#fff",
                            borderRadius: 6,
                            height: '623px',
                            width: '444px',

                        }
                    }}
                >
                    <DialogTitle>

                        <Grid container >
                            <Grid xs={11} >


                            </Grid>
                            <Grid xs={1}> <IconButton
                                aria-label="close"
                                style={styles.closeButton}
                                onClick={() => this.closeContactModal()}
                            >
                                <CloseIcon />
                            </IconButton></Grid>
                        </Grid>
                    </DialogTitle>
                    <DialogContent style={{ paddingLeft: '48px', display: "flex" }}>
                        <Grid container direction="row" style={{ display: 'flex', justifyContent: 'center' }}>
                            <Grid>
                                <img
                                    src={succ_img}
                                    alt="loginb"
                                    height='210x'
                                    width='254px'


                                />
                            </Grid>
                            <Grid style={{ paddingTop: '1%' }}>
                                <Typography style={{ fontFamily: 'OpenSans-Extrabold', fontSize: '32px', color: '#48d74d' }}>Success!</Typography>
                            </Grid>
                            <Grid style={{ paddingTop: '1%', height: '5%' }}>
                                <Typography style={{ fontFamily: 'OpenSans-Extrabold', fontSize: '22px', color: '#454871' }}>Your Demo account has been</Typography>
                            </Grid>
                            <Grid style={{ paddingTop: '0%' }}>
                                <Typography style={{ fontFamily: 'OpenSans-Extrabold', fontSize: '22px', color: '#454871' }}>created successfully</Typography>
                            </Grid>
                            <Button


                                backgroundColor="transparent"
                                variant="contained"
                                fullWidth
                                style={{
                                    color: "#2f3253",
                                    fontFamily: "OpenSans-Bold",
                                    borderRadius: "4px",
                                    backgroundColor: "#f4d553",
                                    fontSize: "14px",
                                    lineHeight: "19px",
                                    textTransform: "none",
                                    padding: "12px 16px",
                                    marginBottom: "5%"

                                }}
                                onClick={() => { this.props.history.push('/dashbord') }}
                            >

                                CONTINUE
                            </Button>

                        </Grid>



                    </DialogContent>
                </Dialog>
            </Grid>
        );
    }
}
export default OpenDemo1;
