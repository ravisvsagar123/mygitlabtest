import React, { useState, useRef } from "react";
import {
  Grid, Typography, Button, Box, makeStyles,
  Step, Stepper, StepLabel, Checkbox

} from '@material-ui/core'
import CustomTextField from "../ui-atoms/components/CustomTextField"
import Select from "react-select";
import countries from "../ui-config/countries.json"
import cfdimg from '../ui-assets/images/cfd.png'
import empstatus from '../ui-assets/images/empstatus.png'
import cfdpage3 from '../ui-assets/images/cfdpage3.png'
import accbg from '../ui-assets/images/Rectangle.svg'
import uploadimg from '../ui-assets/images/upload.svg'
import selectedfile from '../ui-assets/images/selectedfile.svg'
import QRCode from 'qrcode.react';
//import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import ArrowBack from "@material-ui/icons/KeyboardArrowLeft"
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';




export default function OpenRealAccount() {
  const steps = ['Personal Info', ' Account Type', 'Documents', 'Industry Exp'];
  const [activeStep, setActiveStep] = React.useState(0);

  const [fname, setfname] = React.useState('');



  const handleNext = () => {
    alert(fname);
    /*if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }
*/

    if (activeStep === 0) {

    }
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    //setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };




  const styles = {
    mainback: {



      height: '100vh',



      background: 'linear-gradient(119deg, #00cfd2, #006aab 91%, #0060a7)',
    },
    paperContainer: {
      height: '768px',
      width: 'auto',
      backgroundImage: `url(${accbg})`,

      backgroundPosition: 'center',
      margin: '0px 0px',
      backgroundSize: "cover",
    },
    mainbacksub: {
      width: '830px',
      height: '623px',
      background: '#fff',
      borderRadius: '12px',
      justifyContent: 'center',

      margin: '10px 75px 1px 268px'
    },
    mainheadersub: {
      color: "#ffffff",
      fontFamily: "OpenSans-Regular",
      fontSize: "18px",

    },
    demohead: {
      color: "#333333",
      fontFamily: "OpenSans-Regular",
      fontSize: "18px",

    },
    line: {
      border: '#dddddd 1px solid',
    },
    field: {
      marginLeft: "2px",
      marginRight: "2px",
      height: '34px',
      width: '300px',

    },
    mainheader: {
      color: "#ffffff",
      fontFamily: "OpenSans-Bold",
      fontSize: "24px",
    },
    labelContainer: {
      "& $alternativeLabel": {
        marginTop: 0
      }
    },

  };

  const useStyles = makeStyles(() => ({
    root: {
      "& .MuiStepIcon-active": { color: "#f4d553" },
      "& .MuiStepIcon-completed": { color: "#48d74d" },
      "& .Mui-disabled .MuiStepIcon-root": { color: "#ffffff" },
      "& .Mui-disabled .MuiStepLabel-iconContainer": { color: "red" },


    },
    text: {
      color: '#D3D3D3',
    },
  }));


  function getStepContent(step) {
    switch (step) {
      case 0:
        return <PersonalInfo />;
      case 1:
        return <AccountType />;
      case 2:
        return <Documents />;
      case 3:
        return <IndustryExp />;

      default:
        return <Other />;
    }


  }

  function Other(props) {
    return (
      <div>
        <h1>abc</h1>
      </div>
    )

  }

  function IndustryExp(props) {
    const indstyles = {
      mylineborder: {
        border: '1px solid #b6d9b6 ',
        borderRadius: '4px',
        height: '5px',
        width: '48px',
        cursor: "pointer",
      },
      mylinefill: {

        borderRadius: '4px',
        height: '8px',
        width: '48px',
        backgroundColor: "#48d74d",
        cursor: "pointer",
      }
    };

    function Mytab1Click() {
      setMytab1(indstyles.mylinefill)
      setMytab2(indstyles.mylineborder)
      setMytab3(indstyles.mylineborder)
      setMytab4(indstyles.mylineborder)
      setMytab5(indstyles.mylineborder)
      setMytab6(indstyles.mylineborder)
      setMytab7(indstyles.mylineborder)
      setMytab8(indstyles.mylineborder)
      setMytab9(indstyles.mylineborder)
      setMytab10(indstyles.mylineborder)
      setPageNo('1')
    }

    function Mytab2Click() {
      setMytab1(indstyles.mylineborder)
      setMytab2(indstyles.mylinefill)
      setMytab3(indstyles.mylineborder)
      setMytab4(indstyles.mylineborder)
      setMytab5(indstyles.mylineborder)
      setMytab6(indstyles.mylineborder)
      setMytab7(indstyles.mylineborder)
      setMytab8(indstyles.mylineborder)
      setMytab9(indstyles.mylineborder)
      setMytab10(indstyles.mylineborder)
      setPageNo('2')
    }
    function Mytab3Click() {
      setMytab1(indstyles.mylineborder)
      setMytab2(indstyles.mylineborder)
      setMytab3(indstyles.mylinefill)
      setMytab4(indstyles.mylineborder)
      setMytab5(indstyles.mylineborder)
      setMytab6(indstyles.mylineborder)
      setMytab7(indstyles.mylineborder)
      setMytab8(indstyles.mylineborder)
      setMytab9(indstyles.mylineborder)
      setMytab10(indstyles.mylineborder)
      setPageNo('3')
    }
    function Mytab4Click() {
      setMytab1(indstyles.mylineborder)
      setMytab2(indstyles.mylineborder)
      setMytab3(indstyles.mylineborder)
      setMytab4(indstyles.mylinefill)
      setMytab5(indstyles.mylineborder)
      setMytab6(indstyles.mylineborder)
      setMytab7(indstyles.mylineborder)
      setMytab8(indstyles.mylineborder)
      setMytab9(indstyles.mylineborder)
      setMytab10(indstyles.mylineborder)
      setPageNo('4')
    }
    function Mytab5Click() {
      setMytab1(indstyles.mylineborder)
      setMytab2(indstyles.mylineborder)
      setMytab3(indstyles.mylineborder)
      setMytab4(indstyles.mylineborder)
      setMytab5(indstyles.mylinefill)
      setMytab6(indstyles.mylineborder)
      setMytab7(indstyles.mylineborder)
      setMytab8(indstyles.mylineborder)
      setMytab9(indstyles.mylineborder)
      setMytab10(indstyles.mylineborder)
      setPageNo('5')
    }
    function Mytab6Click() {
      setMytab1(indstyles.mylineborder)
      setMytab2(indstyles.mylineborder)
      setMytab3(indstyles.mylineborder)
      setMytab4(indstyles.mylineborder)
      setMytab5(indstyles.mylineborder)
      setMytab6(indstyles.mylinefill)
      setMytab7(indstyles.mylineborder)
      setMytab8(indstyles.mylineborder)
      setMytab9(indstyles.mylineborder)
      setMytab10(indstyles.mylineborder)
      setPageNo('6')
    }
    function Mytab7Click() {
      setMytab1(indstyles.mylineborder)
      setMytab2(indstyles.mylineborder)
      setMytab3(indstyles.mylineborder)
      setMytab4(indstyles.mylineborder)
      setMytab5(indstyles.mylineborder)
      setMytab6(indstyles.mylineborder)
      setMytab7(indstyles.mylinefill)
      setMytab8(indstyles.mylineborder)
      setMytab9(indstyles.mylineborder)
      setMytab10(indstyles.mylineborder)
      setPageNo('7')
    }
    function Mytab8Click() {
      setMytab1(indstyles.mylineborder)
      setMytab2(indstyles.mylineborder)
      setMytab3(indstyles.mylineborder)
      setMytab4(indstyles.mylineborder)
      setMytab5(indstyles.mylineborder)
      setMytab6(indstyles.mylineborder)
      setMytab7(indstyles.mylineborder)
      setMytab8(indstyles.mylinefill)
      setMytab9(indstyles.mylineborder)
      setMytab10(indstyles.mylineborder)
      setPageNo('8')
    }
    function Mytab9Click() {
      setMytab1(indstyles.mylineborder)
      setMytab2(indstyles.mylineborder)
      setMytab3(indstyles.mylineborder)
      setMytab4(indstyles.mylineborder)
      setMytab5(indstyles.mylineborder)
      setMytab6(indstyles.mylineborder)
      setMytab7(indstyles.mylineborder)
      setMytab8(indstyles.mylineborder)
      setMytab9(indstyles.mylinefill)
      setMytab10(indstyles.mylineborder)
      setPageNo('9')
    }
    function Mytab10Click() {
      setMytab1(indstyles.mylineborder)
      setMytab2(indstyles.mylineborder)
      setMytab3(indstyles.mylineborder)
      setMytab4(indstyles.mylineborder)
      setMytab5(indstyles.mylineborder)
      setMytab6(indstyles.mylineborder)
      setMytab7(indstyles.mylineborder)
      setMytab8(indstyles.mylineborder)
      setMytab9(indstyles.mylineborder)
      setMytab10(indstyles.mylinefill)
      setPageNo('10')
    }

    const [mytab1, setMytab1] = useState(indstyles.mylinefill);
    const [mytab2, setMytab2] = useState(indstyles.mylineborder);
    const [mytab3, setMytab3] = useState(indstyles.mylineborder);
    const [mytab4, setMytab4] = useState(indstyles.mylineborder);
    const [mytab5, setMytab5] = useState(indstyles.mylineborder);
    const [mytab6, setMytab6] = useState(indstyles.mylineborder);
    const [mytab7, setMytab7] = useState(indstyles.mylineborder);
    const [mytab8, setMytab8] = useState(indstyles.mylineborder);
    const [mytab9, setMytab9] = useState(indstyles.mylineborder);
    const [mytab10, setMytab10] = useState(indstyles.mylineborder);
    const [mypage, setPageNo] = useState('1');
    return (


      <div>
        <Grid style={styles.line}></Grid>
        <Grid container display='flex' justifyContent='center' style={{ paddingTop: '35px' }}>
          <Grid container display='flex' justifyContent='center' style={{ width: '734px', height: '5px' }}>
            <Grid container item xs={1}>
              <Box style={mytab1} onClick={Mytab1Click}></Box>
            </Grid>
            <Grid container item xs={1}>
              <Box style={mytab2} onClick={Mytab2Click}></Box>
            </Grid>
            <Grid container item xs={1}>
              <Box style={mytab3} onClick={Mytab3Click}></Box>
            </Grid>
            <Grid container item xs={1}>
              <Box style={mytab4} onClick={Mytab4Click}></Box>
            </Grid>
            <Grid container item xs={1}>
              <Box style={mytab5} onClick={Mytab5Click}></Box>
            </Grid>
            <Grid container item xs={1}>
              <Box style={mytab6} onClick={Mytab6Click}></Box>
            </Grid>
            <Grid container item xs={1}>
              <Box style={mytab7} onClick={Mytab7Click}></Box>
            </Grid>
            <Grid container item xs={1}>
              <Box style={mytab8} onClick={Mytab8Click}></Box>
            </Grid>
            <Grid container item xs={1}>
              <Box style={mytab9} onClick={Mytab9Click}></Box>
            </Grid>
            <Grid container item xs={1}>
              <Box style={mytab10} onClick={Mytab10Click}></Box>
            </Grid>

          </Grid>

        </Grid>
        <Grid>
          {mypage === '1' && <Page1 pageno={setPageNo} p1={setMytab1} p2={setMytab2} />}
          {mypage === '2' && <Page2 pageno={setPageNo} p2={setMytab2} p3={setMytab3} />}
          {mypage === '3' && <Page3 pageno={setPageNo} p3={setMytab3} p4={setMytab4} />}
          {mypage === '4' && <Page4 pageno={setPageNo} p4={setMytab4} p5={setMytab5} />}
          {mypage === '5' && <Page5 pageno={setPageNo} p5={setMytab5} p6={setMytab6} />}
          {mypage === '6' && <Page6 pageno={setPageNo} p6={setMytab6} p7={setMytab7} />}
          {mypage === '7' && <Page7 pageno={setPageNo} p7={setMytab7} p8={setMytab8} />}
          {mypage === '8' && <Page8 pageno={setPageNo} p8={setMytab8} p9={setMytab9} />}
          {mypage === '9' && <Page9 pageno={setPageNo} p9={setMytab9} p10={setMytab10} />}
          {mypage === '10' && <Page10 pageno={setPageNo} p9={setMytab9} p10={setMytab10} />}
        </Grid>
      </div>
    )
  }
  function Page1(props) {

    const indstyles = {
      mylineborder: {
        border: '1px solid #b6d9b6 ',
        borderRadius: '4px',
        height: '5px',
        width: '48px',
        cursor: "pointer",
      },
      mylinefill: {

        borderRadius: '4px',
        height: '8px',
        width: '48px',
        backgroundColor: "#48d74d",
        cursor: "pointer",
      }
    };
    function cp() {

      props.pageno('2');
      props.p1(indstyles.mylineborder);
      props.p2(indstyles.mylinefill);
    }

    return (
      <Grid>
        <Grid style={{ paddingTop: '30px', height: '155px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <img
            src={cfdimg}
            alt="loginb"
            height='155px'
            width='131px'
          ></img>
        </Grid>
        <Grid style={{ paddingTop: '18px', height: '15px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Do you know what is a CFD?*</Typography>
        </Grid>
        <Grid style={{ paddingTop: '18px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#666666",
              fontFamily: "OpenSans-Bold",
              borderRadius: "2px",
              fontSize: "12px",
              textTransform: "none",
              width: '300px',
              padding: "2px 2px",
            }}

            onClick={cp}
          >Yes
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '18px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#666666",
              fontFamily: "OpenSans-Bold",
              borderRadius: "2px",
              fontSize: "12px",
              textTransform: "none",
              padding: "2px 2px",
            }}
            onClick={cp}

          >No
          </Button>

        </Grid>
      </Grid>


    )
  }


  function Page2(props) {
    const indstyles = {
      mylineborder: {
        border: '1px solid #b6d9b6 ',
        borderRadius: '4px',
        height: '5px',
        width: '48px',
        cursor: "pointer",
      },
      mylinefill: {

        borderRadius: '4px',
        height: '8px',
        width: '48px',
        backgroundColor: "#48d74d",
        cursor: "pointer",
      }
    };
    function cp() {

      props.pageno('3');
      props.p2(indstyles.mylineborder);
      props.p3(indstyles.mylinefill);
    }
    return (
      <Grid>
        <Grid style={{ paddingTop: '20px', height: '133px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <img
            src={empstatus}
            alt="loginb"
            height='133px'
            width='220px'
          ></img>
        </Grid>
        <Grid style={{ paddingTop: '25px', height: '15px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Employment Status*</Typography>
        </Grid>
        <Grid style={{ paddingTop: '2%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "2px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
            }}
            onClick={cp}

          >I hold a professional Qualification in finance/economics
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '2%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "2px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
            }}

            onClick={cp}
          >Received higher education in finance and/ Or financial services
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '2%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "2px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
            }}
            onClick={cp}
          >In the last 3 years, worked in a role relevant to trading derivatives, For over 1 year
          </Button>

        </Grid>
      </Grid>


    )
  }


  function Page3(props) {
    const indstyles = {
      mylineborder: {
        border: '1px solid #b6d9b6 ',
        borderRadius: '4px',
        height: '5px',
        width: '48px',
        cursor: "pointer",
      },
      mylinefill: {

        borderRadius: '4px',
        height: '8px',
        width: '48px',
        backgroundColor: "#48d74d",
        cursor: "pointer",
      }
    };
    function cp() {

      props.pageno('4');
      props.p3(indstyles.mylineborder);
      props.p4(indstyles.mylinefill);
    }
    return (
      <Grid>
        <Grid style={{ paddingTop: '30px', height: '150px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <img
            src={cfdpage3}
            alt="loginb"
            height='150px'
            width='235px'
          ></img>
        </Grid>
        <Grid style={{ paddingTop: '18px', height: '15px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Trading with a higher leverage in CFD’s
          </Typography>
        </Grid>
        <Grid style={{ paddingTop: '5px', height: '15px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}> Means you may open a larger position volume, Thus increasing the risk of loss*          </Typography>
        </Grid>

        <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '300px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Yes
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '18px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '300px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >No
          </Button>

        </Grid>
      </Grid>


    )
  }
  function Page4(props) {
    const indstyles = {
      mylineborder: {
        border: '1px solid #b6d9b6 ',
        borderRadius: '4px',
        height: '5px',
        width: '48px',
        cursor: "pointer",
      },
      mylinefill: {

        borderRadius: '4px',
        height: '8px',
        width: '48px',
        backgroundColor: "#48d74d",
        cursor: "pointer",
      }
    };
    function cp() {

      props.pageno('5');
      props.p4(indstyles.mylineborder);
      props.p5(indstyles.mylinefill);
    }
    return (
      <Grid>

        <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
          <Grid>
            <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              Over the past 2 years, to what extent have you traded on your own

            </Typography>
            <Typography style={{ paddingTop: '2px', paddingBottom: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              decision the following financial products
            </Typography>
          </Grid>
        </Grid>
        <Grid style={{ paddingTop: '45px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Shares, Bonds, Equities or Exchange Traded Funds</Typography>
        </Grid>
        <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}
            onClick={cp}

          >Never
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}
            onClick={cp}

          >Rarely: 1-25 Trades A Year
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Sometimes: 25-100 Trades A Year
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}
            onClick={cp}

          >Often: Over 100 Trades A Year
          </Button>

        </Grid>
      </Grid>


    )
  }
  function Page5(props) {
    const indstyles = {
      mylineborder: {
        border: '1px solid #b6d9b6 ',
        borderRadius: '4px',
        height: '5px',
        width: '48px',
        cursor: "pointer",
      },
      mylinefill: {

        borderRadius: '4px',
        height: '8px',
        width: '48px',
        backgroundColor: "#48d74d",
        cursor: "pointer",
      }
    };
    function cp() {

      props.pageno('6');
      props.p5(indstyles.mylineborder);
      props.p6(indstyles.mylinefill);
    }
    return (
      <Grid>

        <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
          <Grid>
            <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              Over the past 2 years, to what extent have you traded on your own

            </Typography>
            <Typography style={{ paddingTop: '2px', paddingBottom: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              decision the following financial products
            </Typography>
          </Grid>
        </Grid>
        <Grid style={{ paddingTop: '45px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Derivatives such as Futures, Options, Swaps*</Typography>
        </Grid>
        <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}
            onClick={cp}

          >Never
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}
            onClick={cp}

          >Rarely: 1-25 Trades A Year
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Sometimes: 25-100 Trades A Year
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}
            onClick={cp}

          >Often: Over 100 Trades A Year
          </Button>

        </Grid>
      </Grid>


    )
  }
  function Page6(props) {
    const indstyles = {
      mylineborder: {
        border: '1px solid #b6d9b6 ',
        borderRadius: '4px',
        height: '5px',
        width: '48px',
        cursor: "pointer",
      },
      mylinefill: {

        borderRadius: '4px',
        height: '8px',
        width: '48px',
        backgroundColor: "#48d74d",
        cursor: "pointer",
      }
    };
    function cp() {

      props.pageno('7');
      props.p6(indstyles.mylineborder);
      props.p7(indstyles.mylinefill);
    }
    return (
      <Grid>

        <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
          <Grid>
            <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              Over the past 2 years, to what extent have you traded on your own

            </Typography>
            <Typography style={{ paddingTop: '2px', paddingBottom: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              decision the following financial products
            </Typography>
          </Grid>
        </Grid>
        <Grid style={{ paddingTop: '45px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Forex (FX) Or Rolling Spot FX*</Typography>
        </Grid>
        <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Never
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Rarely: 1-25 Trades A Year
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Sometimes: 25-100 Trades A Year
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}
            onClick={cp}

          >Often: Over 100 Trades A Year
          </Button>

        </Grid>
      </Grid>


    )
  }
  function Page7(props) {
    const indstyles = {
      mylineborder: {
        border: '1px solid #b6d9b6 ',
        borderRadius: '4px',
        height: '5px',
        width: '48px',
        cursor: "pointer",
      },
      mylinefill: {

        borderRadius: '4px',
        height: '8px',
        width: '48px',
        backgroundColor: "#48d74d",
        cursor: "pointer",
      }
    };
    function cp() {

      props.pageno('8');
      props.p7(indstyles.mylineborder);
      props.p8(indstyles.mylinefill);
    }
    return (
      <Grid>

        <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
          <Grid>
            <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              Over the past 2 years, to what extent have you traded on your own

            </Typography>
            <Typography style={{ paddingTop: '2px', paddingBottom: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              decision the following financial products
            </Typography>
          </Grid>
        </Grid>
        <Grid style={{ paddingTop: '45px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>CFDs*</Typography>
        </Grid>
        <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Never
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Rarely: 1-25 Trades A Year
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Sometimes: 25-100 Trades A Year
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Often: Over 100 Trades A Year
          </Button>

        </Grid>
      </Grid>


    )
  }



  function Page8(props) {
    const indstyles = {
      mylineborder: {
        border: '1px solid #b6d9b6 ',
        borderRadius: '4px',
        height: '5px',
        width: '48px',
        cursor: "pointer",
      },
      mylinefill: {

        borderRadius: '4px',
        height: '8px',
        width: '48px',
        backgroundColor: "#48d74d",
        cursor: "pointer",
      }
    };
    function cp() {

      props.pageno('9');
      props.p8(indstyles.mylineborder);
      props.p9(indstyles.mylinefill);
    }
    return (
      <Grid>

        <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
          <Grid>
            <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              Over the past 2 years, to what extent have you traded on your own

            </Typography>
            <Typography style={{ paddingTop: '2px', paddingBottom: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              decision the following financial products
            </Typography>
          </Grid>
        </Grid>
        <Grid style={{ paddingTop: '40px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>What was the average yearly volume of invested amounts of your past</Typography>
        </Grid>
        <Grid style={{ paddingTop: '1px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>transactions in the financial products you selected*</Typography>
        </Grid>


        <Grid style={{ paddingTop: '28px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}
            onClick={cp}

          >Never Invested
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Less than $500
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >$500 - $2,500
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >$2,500 - 10,000
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}
            onClick={cp}

          >Over 10,000
          </Button>

        </Grid>

      </Grid>


    )
  }
  function Page9(props) {
    const indstyles = {
      mylineborder: {
        border: '1px solid #b6d9b6 ',
        borderRadius: '4px',
        height: '5px',
        width: '48px',
        cursor: "pointer",
      },
      mylinefill: {

        borderRadius: '4px',
        height: '8px',
        width: '48px',
        backgroundColor: "#48d74d",
        cursor: "pointer",
      }
    };
    function cp() {

      props.pageno('10');
      props.p9(indstyles.mylineborder);
      props.p10(indstyles.mylinefill);
    }
    return (
      <Grid>

        <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
          <Grid>
            <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              Leagal Information

            </Typography>

          </Grid>
        </Grid>
        <Grid style={{ paddingTop: '45px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Are you A “Politically Exposed person”*</Typography>
        </Grid>
        <Grid style={{ paddingTop: '25px', height: '20px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '16px', color: '#999999' }}>
            Politically exposed persons” means: any person holding prominent public positions
          </Typography>
        </Grid>
        <Grid style={{ paddingTop: '5px', height: '20px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '16px', color: '#999999' }}>

            abroad: heads of state or of government, senior politicians on the national level, senior.
          </Typography>
        </Grid>

        <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >Yes
          </Button>

        </Grid>
        <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button
            backgroundColor="transparent"
            variant="text"
            style={{
              color: "#454871",
              fontFamily: "OpenSans-Bold",
              borderRadius: "16px",
              fontSize: "14px",
              textTransform: "none",
              padding: "2px 2px",
              width: '678px',
              border: '1px solid #dddddd'
            }}

            onClick={cp}
          >No
          </Button>

        </Grid>


      </Grid>


    )
  }


  function Page10(props) {
    return (
      <Grid>

        <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
          <Grid>
            <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              Leagal Information

            </Typography>

          </Grid>
        </Grid>
        <Grid style={{ paddingTop: '15px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Which is the country from which deposit will be transferred </Typography>
        </Grid>

        <Grid container style={{ paddingLeft: '18%', display: 'flex', alignItems: 'center', height: '25px' }}>
          <Grid><Checkbox style={{ Color: 'yellow' }} /></Grid>
          <Grid><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#454871' }}>I agree to the</Typography></Grid>
          <Grid style={{ paddingLeft: '1%' }}><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#0083ca' }}>Terms and Conditions*</Typography></Grid>
        </Grid>
        <Grid container style={{ paddingLeft: '18%', display: 'flex', alignItems: 'center', height: '25px' }}>
          <Grid><Checkbox style={{ Color: 'yellow' }} /></Grid>
          <Grid><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#454871' }}>I agree to the</Typography></Grid>
          <Grid style={{ paddingLeft: '1%' }}><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#0083ca' }}>Privacy Policy*</Typography></Grid>
        </Grid>
        <Grid container style={{ paddingLeft: '18%', display: 'flex', alignItems: 'center', height: '25px' }}>
          <Grid><Checkbox style={{ Color: 'yellow' }} /></Grid>
          <Grid><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#454871' }}>I agree to the</Typography></Grid>
          <Grid style={{ paddingLeft: '1%' }}><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#0083ca' }}>Risk Disclosure*</Typography></Grid>
        </Grid>
        <Grid container style={{ paddingLeft: '18%', display: 'flex', alignItems: 'center', height: '25px' }}>
          <Grid><Checkbox style={{ Color: 'yellow' }} /></Grid>
          <Grid><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#454871' }}>I understand and agree with the terms of this contract.</Typography></Grid>


        </Grid>
        <Grid container style={{ paddingLeft: '19.5%', display: 'flex', alignItems: 'center', width: '91%', paddingTop: '3.2%' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '14px', color: '#454871' }}>
            You should not agree or sign the application from if you are unsure as to the effects of
          </Typography>
        </Grid>
        <Grid container style={{ paddingLeft: '19.5%', display: 'flex', alignItems: 'center', width: '91%' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '14px', color: '#454871' }}>
            the terms of busness or the nature of the risk involved, By signing the risk warning you
          </Typography>
        </Grid>
        <Grid container style={{ paddingLeft: '19.5%', display: 'flex', alignItems: 'center', width: '91%' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '14px', color: '#454871' }}>
            are acknowledging to bullforce tha you have recived, read and understood that you
          </Typography>
        </Grid>
        <Grid container style={{ paddingLeft: '19.5%', display: 'flex', alignItems: 'center', width: '91%' }}>
          <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '14px', color: '#454871' }}>
            have recived read and understood the contents of the risk warning inits entry
          </Typography>
        </Grid>
      </Grid>


    )
  }
  function AccountType(props) {
    const select_style = {
      control: base => ({
        ...base,
        border: 0,
        // This line disable the blue border
        boxShadow: "none"
      })
    };
    const empst = [
      { value: 'Employee', label: 'Employee' },
      { value: 'Salaried_employee', label: 'Salaried Employee' },
      { value: 'Self-employed', label: 'Self employed' },
      { value: 'Entrepreneur', label: 'Entrepreneur' },
      { value: 'Employee_Enterpreneus', label: 'Employee & Enterpreneus' },
      { value: 'Unemployed', label: 'Unemployed' },
      { value: 'Student', label: 'Student' },
      { value: 'Retired', label: 'Retired' },
    ]


    function handleDateChange(date) {


      SetselectedDate(date);
    }

    const [selectedDate, SetselectedDate] = useState(new Date());
    return (
      <div>
        <Grid style={styles.line}></Grid>
        <Grid container direction="row" style={{ height: '95%', display: 'flex', justifyContent: 'center' }}>
          <Grid>
            <Grid style={{ display: 'flex', paddingTop: '10px' }}>
              <Grid item xs={4} style={{ fontSize: '12px', colour: '#666666', fontFamily: 'OpenSans-Regular', display: 'flex', alignItems: 'center', justifyContent: 'left' }}>Date Of Birth*</Grid>
              <Grid item xs={8}>
                <MuiPickersUtilsProvider

                  style={{ height: '20px' }} utils={DateFnsUtils}>
                  <KeyboardDatePicker

                    value={selectedDate}
                    onChange={date => { handleDateChange(date) }}
                    format="yyyy/MM/dd"

                    inputVariant="standard"
                    PopoverProps={{
                      style: { backgroundColor: 'red' }
                    }}
                    KeyboardButtonProps={{
                      style: { marginLeft: -24 }
                    }}

                  />
                </MuiPickersUtilsProvider>

              </Grid>
            </Grid>

            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Place of Birth*"
              />
            </Grid>

            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Nationality*"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Citizenship"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Education level"
              />

            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select styles={select_style} options={empst} style={{ width: '300px', height: '45px' }} placeholder="Employment Status" />
            </Grid>

            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Profession"
              />

            </Grid>
          </Grid>
        </Grid>
        <Grid style={{ paddingTop: '3%' }}></Grid>
      </div>
    )
  }


  function Documents(props) {
    const select_style = {
      control: base => ({
        ...base,
        border: 0,
        // This line disable the blue border
        boxShadow: "none",
        fontSize: '14px',
        fontFamily: 'OpenSans-Regular',
        colour: '#999999'
      })
    };

    const profofind = [
      { value: 'Passport', label: 'Passport' },
      { value: 'National_ID', label: 'National ID' },


    ]
    const addverf = [
      { value: 'Credit_Card_Statement', label: 'Credit Card Statement' },
      { value: 'Bank_Statement', label: 'Bank Statement' },
      { value: 'Water_Bill', label: 'Water Bill' },
      { value: 'Phone_Bill', label: 'Phone Bill' },
      { value: 'Electricity_Bill', label: 'Electricity Bill' },
    ]
    const otherdoc = [
      { value: 'DTA', label: 'DTA' },
      { value: 'Credit_Card', label: 'Credit Card' },
      { value: 'Other_Documents', label: 'Other Documents' },

    ]

    function fileSelectHandler1(e) {
      setFileName1(e.target.files[0].name);
      setVisible1(true);

    } function fileSelectHandler2(e) {
      setFileName2(e.target.files[0].name);
      setVisible2(true);



    }
    function fileSelectHandler3(e) {
      setFileName3(e.target.files[0].name);
      setVisible3(true);


    }

    function DeleteFile1() {
      setFileName1(null);
      setVisible1(false);


    }

    function DeleteFile2() {
      setFileName2(null);
      setVisible2(false);


    }

    function DeleteFile3() {
      setFileName3(null);
      setVisible3(false);


    }
    const [fname1, setFileName1] = useState(null);
    const [fname2, setFileName2] = useState(null);
    const [fname3, setFileName3] = useState(null);
    const [visible1, setVisible1] = React.useState(false);
    const [visible2, setVisible2] = React.useState(false);
    const [visible3, setVisible3] = React.useState(false);
    return (
      <div>
        <Grid style={styles.line}></Grid>

        <Grid container direction="row" style={{ height: '95%', paddingTop: '25px', marginLeft: '20px' }}>

          <Grid container style={{ height: '35px' }}>

            <Grid container style={{ justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
              <Grid item xs={3}>    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#666666' }}>Proof of Identification*</Typography></Grid>
              <Grid item xs={4}> <Select
                maxMenuHeight="26vh"
                styles={select_style}
                placeholder="Select an option"
                options={profofind}
              /></Grid>
              <Grid container display='flex' item xs={2}>
                <Grid item xs={2}>
                  <img
                    src={uploadimg}
                    alt="filetype"
                    height='20px'
                    width='16px'
                  ></img>
                </Grid>
                <Grid item xs={10}>

                  <input
                    accept="image/*"

                    style={{ display: 'none' }}
                    id="raised-button-file1"
                    multiple
                    type="file"
                    onChange={fileSelectHandler1}
                  />
                  <label htmlFor="raised-button-file1">
                    <Button
                      backgroundColor="transparent"
                      variant="text"
                      component="span"
                      style={{
                        color: "#666666",
                        fontFamily: "OpenSans-Bold",
                        borderRadius: "2px",
                        fontSize: "12px",
                        textTransform: "none",
                        padding: "2px 2px",


                      }}



                    >Choose a file
                    </Button>
                  </label>
                </Grid>
              </Grid>
              <Grid container display='flex' item xs={3}>
                <Grid item xs={2}>
                  {visible1 &&
                    <img
                      src={selectedfile}
                      alt="filetype"
                      height='20px'
                      width='16px'
                    ></img>
                  }
                </Grid>
                <Grid item xs={7}>
                  <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Regular', color: '#666666' }}>{fname1}</Typography>
                </Grid>
                {visible1 &&
                  <Grid item xs={1}>
                    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#00000', cursor: "pointer", }}
                      onClick={DeleteFile1}
                    >X</Typography>
                  </Grid>
                }
              </Grid>
            </Grid>
          </Grid>
          <Grid container style={{ height: '35px' }}>

            <Grid container style={{ justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
              <Grid item xs={3}>    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#666666' }}>Address Verification*</Typography></Grid>
              <Grid item xs={4}>
                <Select
                  maxMenuHeight="26vh"
                  styles={select_style}
                  placeholder="Select an option"
                  options={addverf}
                /></Grid>
              <Grid container display='flex' item xs={2}>
                <Grid item xs={2}>
                  <img
                    src={uploadimg}
                    alt="filetype"
                    height='20px'
                    width='16px'
                  ></img>
                </Grid>
                <Grid item xs={10}>
                  <input
                    accept="image/*"

                    style={{ display: 'none' }}
                    id="raised-button-file2"
                    multiple
                    type="file"
                    onChange={fileSelectHandler2}
                  />
                  <label htmlFor="raised-button-file2">
                    <Button
                      backgroundColor="transparent"
                      variant="text"
                      component="span"
                      style={{
                        color: "#666666",
                        fontFamily: "OpenSans-Bold",
                        borderRadius: "2px",
                        fontSize: "12px",
                        textTransform: "none",
                        padding: "2px 2px",
                      }}


                    >Choose a file
                    </Button>
                  </label>
                </Grid>
              </Grid>
              <Grid container display='flex' item xs={3}>
                <Grid item xs={2}>
                  {visible2 &&
                    <img
                      src={selectedfile}
                      alt="filetype"
                      height='20px'
                      width='16px'
                    ></img>
                  }
                </Grid>
                <Grid item xs={7}>
                  <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Regular', color: '#666666' }}>{fname2}</Typography>
                </Grid>
                {visible2 &&
                  <Grid item xs={1}>
                    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#00000', cursor: "pointer", }}
                      onClick={DeleteFile2}
                    >X</Typography>
                  </Grid>
                }
              </Grid>
            </Grid>
          </Grid>
          <Grid container style={{ height: '35px' }}>

            <Grid container style={{ justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
              <Grid item xs={3}>    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#666666' }}>Other Documents*</Typography></Grid>
              <Grid item xs={4}> <Select
                maxMenuHeight="26vh"
                styles={select_style}
                placeholder="Select an option"
                options={otherdoc}
              /></Grid>
              <Grid container display='flex' item xs={2}>
                <Grid item xs={2}>

                  <img
                    src={uploadimg}
                    alt="filetype"
                    height='20px'
                    width='16px'
                  ></img>

                </Grid>
                <Grid item xs={10}>
                  <input
                    accept="image/*"

                    style={{ display: 'none' }}
                    id="raised-button-file3"
                    multiple
                    type="file"
                    onChange={fileSelectHandler3}
                  />
                  <label htmlFor="raised-button-file3">
                    <Button
                      backgroundColor="transparent"
                      variant="text"
                      component="span"
                      style={{
                        color: "#666666",
                        fontFamily: "OpenSans-Bold",
                        borderRadius: "2px",
                        fontSize: "12px",
                        textTransform: "none",
                        padding: "2px 2px",
                      }}


                    >Choose a file
                    </Button>
                  </label>
                </Grid>
              </Grid>

              <Grid container display='flex' item xs={3}>
                <Grid item xs={2}>
                  {visible3 &&
                    <img
                      src={selectedfile}
                      alt="filetype"
                      height='20px'
                      width='16px'
                    ></img>
                  }
                </Grid>
                <Grid item xs={7}>
                  <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Regular', color: '#666666' }}>{fname3}</Typography>
                </Grid>
                {visible3 &&
                  <Grid item xs={1}>
                    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#00000', cursor: "pointer", }}
                      onClick={DeleteFile3}
                    >X</Typography>
                  </Grid>
                }
              </Grid>

              <Grid>

              </Grid>

            </Grid>

          </Grid>

        </Grid>
        <Grid style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>

          <QRCode
            value="https://www.bullforce.co/" style={{ paddingTop: '100px' }} />
        </Grid>
      </div >
    )
  }


  function PersonalInfo(props) {
    const select_style = {
      control: base => ({
        ...base,
        border: 0,

        boxShadow: "none",
        fontSize: '14px',
        fontFamily: 'OpenSans-Regular',
        colour: '#999999'
      })
    };
    const countriesData = countries.map(data => {
      return { label: data.country, value: data.countryISO2 };
    });

    const acctype = [
      { value: 'STANDARD', label: 'STANDARD' },
      { value: 'ELITE', label: 'ELITE' },
      { value: 'PREMIUM', label: 'PREMIUM' },
    ]

    const cltype = [
      { value: 'Individual_Account', label: 'Individual Account' },
      { value: 'Common_Account', label: 'Common Account' },
      { value: 'Corporate_Account', label: ' Corporate Account' },
    ]

    const currtype = [
      { value: 'EUR', label: 'EUR' },
      { value: 'USD', label: 'USD' },

    ]

    return (
      <div>

        <Grid style={styles.line}></Grid>
        <Grid container style={{ height: '95%' }}>
          <Grid item xs={1}></Grid>

          <Grid item xs={4}>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                name='title'
                style={styles.field}

                placeholder="First Name*"

              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}

                style={styles.field}
                placeholder="Last Name*"

              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Email*"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Mobile No*"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Address"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Postal code"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px', width: '300px', height: '45px' }}>
              <Select styles={select_style} options={countriesData} style={{ width: '300px', height: '45px' }} placeholder="Country" />
            </Grid>
          </Grid>

          <Grid item xs={1}></Grid>
          <Grid item xs={4}>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="State"
              />

            </Grid>
            <Grid style={{ paddingTop: '10px' }} >

              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="City"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select styles={select_style} options={currtype} style={{ width: '300px', height: '45px' }} placeholder="Currency choice" />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select styles={select_style} options={acctype} style={{ width: '300px', height: '45px' }} placeholder="Account Type" />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select styles={select_style} options={cltype} style={{ width: '300px', height: '45px' }} placeholder=" Client Type" />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Password*"
                fullWidth={true}
                type="Password"
                hasEndAdornment={true}
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Password Confirmation*"
                fullWidth={true}
                type="Password*"
                hasEndAdornment={true}
              />
            </Grid>

          </Grid>

          <Grid item xs={1}></Grid>



        </Grid>
      </div >
    );
  }
  const c = useStyles();
  return (
    <Grid style={styles.paperContainer}>
      <Grid style={{ justifyContent: 'center', display: 'flex' }}>
        <Typography style={styles.mainheader}>Registration. Use form below</Typography>
      </Grid>
      <Grid style={{ justifyContent: 'center', display: 'flex' }}>
        <Typography style={styles.mainheadersub}>Please fill in your details to create a trading account.</Typography>
      </Grid>

      <Grid direction='row' style={styles.mainbacksub}>
        <Grid style={{ height: '11%', marginRight: '2%', marginLeft: '2%', alignSelf: 'start' }}>
          <Stepper className={c.root} activeStep={activeStep} style={{ paddingTop: '15px', borderRadius: '12px' }}>
            {steps.map((label, index) => {
              const stepProps = {};
              const labelProps = {};


              return (
                <Step key={label} {...stepProps}>
                  <StepLabel  {...labelProps}>{label}</StepLabel>
                </Step>
              );
            })}
          </Stepper>
        </Grid>
        <Grid style={{ height: '78%', alignSelf: 'center' }}>
          {getStepContent(activeStep)}
        </Grid>
        <Grid style={{ display: 'flex', height: '11%', marginRight: '7%', marginLeft: '7%', alignSelf: 'end' }}>
          <Grid item xs={8} style={{ paddingTop: '1%' }}>
            <Button
              backgroundColor="transparent"
              variant="text"
              style={{
                color: "#2f3253",
                fontFamily: "OpenSans-Bold",
                borderRadius: "4px",

                fontSize: "14px",
                lineHeight: "19px",
                textTransform: "none",
                padding: "12px 16px",
                marginBottom: "5%"

              }}

              disabled={activeStep === 0}
              onClick={handleBack}
              sx={{ mr: 1 }}
              startIcon={<ArrowBack />}
            >BACK
            </Button></Grid>
          <Grid item xs={2} style={{ paddingTop: '1%' }}>
            <Button
              backgroundColor="transparent"
              variant="outlined"
              fullWidth
              style={{
                color: "#2f3253",
                fontFamily: "OpenSans-Bold",
                borderRadius: "4px",
                border: '#454871 1px solid',
                fontSize: "14px",
                lineHeight: "19px",
                textTransform: "none",
                padding: "12px 16px",
                marginBottom: "5%",
                width: '82px'
              }}>SAVE
            </Button>
          </Grid>
          <Grid item xs={2} style={{ paddingTop: '1%' }}>
            <Button
              backgroundColor="transparent"
              variant="contained"
              fullWidth
              style={{
                color: "#2f3253",
                fontFamily: "OpenSans-Bold",
                borderRadius: "4px",
                backgroundColor: "#f4d553",
                fontSize: "14px",
                lineHeight: "19px",
                textTransform: "none",
                padding: "12px 16px",
                marginBottom: "5%"

              }}
              onClick={handleNext}
            >NEXT
            </Button>
          </Grid>

        </Grid>

      </Grid>
    </Grid >

  )
}
