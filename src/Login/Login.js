import React, { Component } from "react";
import { Grid, Typography, Button, Snackbar, IconButton } from "@material-ui/core"
import bg from '../ui-assets/images/register-bg.jpeg'
import CustomTextField from "../ui-atoms/components/CustomTextField"
import mylogo from '../ui-assets/images/logo.svg'
import signupimg from '../ui-assets/images/CFD_bg.png'
import CloseIcon from '@material-ui/icons/Close'

import validator from 'validator'

const styles = {
  paperContainer: {
    height: '100vh',
    backgroundImage: `url(${bg})`,

    backgroundPosition: 'center',
    margin: '0px 0px',
    backgroundSize: 'cover',
  },
  loadlogo: {

    backgroundImage: `url(${mylogo})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: '100%',
  },
  sigingrid: {

    width: '482px',
    height: '442px',
    backgroundSize: 'cover',
    backgroundImage: `url(${signupimg})`,
    marginTop: '15%'

  },
  contpadd: {
    margin: '62px 100px 10px 92px',

    width: '400px',
    height: '220px',
  },
  contpadd1: {
    margin: '230px 149px 10px 92px',

    width: '400px',
    height: '220px',
  },
  loginhead: {
    color: "#fff",
    fontFamily: " OpenSans-Extrabold",
    fontSize: "22px",

  },
  loginsubhead: {
    color: "#dddddd",
    fontFamily: "OpenSans-Regular",
    fontSize: "16px",
  },
  field: {
    marginLeft: "2px",
    marginRight: "2px",
    height: '45px',

  },
  loginwithpass: {
    color: "#f4d553",
    fontFamily: "OpenSans-Bold",
    fontSize: "14px",
    cursor: "pointer",

  },
  haveaccount: {
    color: "#ffffff",
    fontFamily: "OpenSans",
    fontSize: "16px",
  },
  signup: {
    color: "#f4d553",
    fontFamily: "OpenSans-Bold",
    fontSize: "14px",
    cursor: "pointer",
  },
  welcomtxt: {
    color: "#25c9d4",
    fontFamily: "OpenSans-Extrabold",
    fontSize: "24px",


  },
  subtit: {
    color: "#eeeeee",
    fontFamily: "OpenSans-Regular",
    fontSize: "22px",
  }

};


export class LoginLayout extends Component {




  handleToClose() {

    this.setState({ open: false })



  }

  routeChange = () => {
    var emailid = this.state.emailid;

    var isok = '0';

    if (!emailid.length) {
      isok = 1;

      this.setState({ open: true, errorMsg: 'Email ID Rerequired' })



    }
    if (!validator.isEmail(emailid) && isok === '0') {
      isok = 1;
      this.setState({ open: true, errorMsg: 'Enter valid Email!' })

    }

    if (isok === '0') {

      this.props.history.push("/otp", this.state);


    }

  }
  handleLoginwithpass() {
    var emailid = this.state.emailid;

    var isok = '0';

    if (!emailid.length) {
      isok = 1;

      this.setState({ open: true, errorMsg: 'Email ID Rerequired' })



    }
    if (!validator.isEmail(emailid) && isok === '0') {
      isok = 1;
      this.setState({ open: true, errorMsg: 'Enter valid Email!' })

    }

    if (isok === '0') {
      this.props.history.push("/login", this.state);

    }
  }
  constructor(props) {
    super(props)

    this.state = {
      emailid: 'ravi@bullforce.co',
      open: false,
      errorMsg: ''

    }
  }

  handleChange = (e) => this.setState({
    emailid: e.target.value
  })
  render() {
    const { emailid, open, errorMsg } = this.state


    return (

      <div >

        <Grid container style={styles.paperContainer}>

          <Grid item xs={6}
          >


            <Grid direction="row" item xs={8} style={{ marginTop: '198px', marginLeft: '149px' }}>

              <img
                src={mylogo}
                alt="loginb"
                height='160px'
                width='198px'


              />
              <Grid style={{ paddingTop: '20px' }}><Typography style={styles.welcomtxt}>
                Welcome to CFD Trading</Typography>
              </Grid>
              <Grid>
                <Typography style={styles.subtit}>This is an Artificial Intelligence-based
                  prediction engine applicable for
                  demonstrated by machines, unlike</Typography>
              </Grid>
            </Grid>



          </Grid>
          <Grid item xs={6}>
            <Grid container item xs={12} direction="row" style={styles.sigingrid}>

              <Grid style={styles.contpadd}><Typography style={styles.loginhead}>Login with CFD</Typography>
                <Grid style={{ margin: '20px 0px 0px 0px' }}><Typography style={styles.loginsubhead}>Enter your email id</Typography></Grid>

                <Grid style={{ margin: '20px 0px 0px 0px' }}>
                  <CustomTextField
                    autoFocus={true}
                    value={emailid}
                    style={styles.field}
                    placeholder="Email address"
                    fullWidth={true}
                    onChange={this.handleChange}


                  />
                </Grid>
                <Grid style={{ margin: '10px 0px 0px 0px' }}>
                  <Typography style={styles.loginwithpass}
                    onClick={() => {
                      this.handleLoginwithpass()
                    }}>LOGIN WITH PASSWORD</Typography>
                </Grid>
                <Grid style={{ margin: '50px 0px 0px 0px' }}>
                  <Button
                    fullWidth


                    variant="contained"
                    size="large"
                    style={{
                      color: "#2f3253",
                      fontFamily: "OpenSans-Bold",
                      borderRadius: "4px",
                      backgroundColor: "#f4d553",

                      fontSize: "14px",
                      lineHeight: "19px",
                      textTransform: "none",
                      padding: "12px 16px",
                      marginBottom: "5%"

                    }}
                    onClick={() => {
                      this.routeChange()
                    }}
                  >

                    LOGIN WITH OTP
                  </Button>
                </Grid>

              </Grid>
              <Grid style={{ margin: '0px 0px 0px 92px' }}><Typography style={styles.haveaccount}>Don't have an account</Typography> </Grid>
              <Grid style={{ margin: '0px 0px 0px 25px' }}><Typography style={styles.signup} onClick={() =>
                this.props.history.push("/signup", this.state)
              }>SIGNUP</Typography></Grid>
            </Grid>

          </Grid>
        </Grid>


        <Snackbar

          anchorOrigin={{
            horizontal: "right",
            vertical: "bottom",
          }}
          open={open}
          autoHideDuration={50}
          message={errorMsg}

          action={
            <React.Fragment>

              <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={() => { this.handleToClose() }}
              >
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
      </div >
    );
  }
}

export default LoginLayout;