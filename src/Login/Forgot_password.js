import React, { Component } from "react";
import { Grid, Typography, Button, Snackbar, IconButton } from "@material-ui/core"
import bg from '../ui-assets/images/register-bg.jpeg'
import CustomTextField from "../ui-atoms/components/CustomTextField"
import mylogo from '../ui-assets/images/logo.svg'
import signupimg from '../ui-assets/images/CFD_bg.png'
import CloseIcon from '@material-ui/icons/Close'
import ArrowBack from "@material-ui/icons/KeyboardArrowLeft"
import OtpInput from "react-otp-input";
const styles = {
  paperContainer: {
    height: '100vh',
    backgroundImage: `url(${bg})`,

    backgroundPosition: 'center',
    margin: '0px 0px',
    backgroundSize: 'cover',
  },
  loadlogo: {

    backgroundImage: `url(${mylogo})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: '100%',
  },
  sigingrid: {

    width: '482px',
    height: '442px',
    backgroundSize: 'cover',
    backgroundImage: `url(${signupimg})`,
    marginTop: '15%'

  },
  contpadd: {
    margin: '40px 100px 10px 92px',

    width: '400px',
    height: '220px',
  },
  contpadd1: {
    margin: '230px 149px 10px 92px',

    width: '400px',
    height: '220px',
  },
  loginhead: {
    color: "#fff",
    fontFamily: " OpenSans-Extrabold",
    fontSize: "22px",

  },
  loginsubhead: {
    color: "#dddddd",
    fontFamily: "OpenSans-Regular",
    fontSize: "16px",
  },
  field: {
    marginLeft: "2px",
    marginRight: "2px",
    height: '45px',

  },
  loginwithpass: {
    color: "#f4d553",
    fontFamily: "OpenSans-Bold",
    fontSize: "14px",
    cursor: "pointer",

  },
  haveaccount: {
    color: "#ffffff",
    fontFamily: "OpenSans",
    fontSize: "16px",
  },
  signup: {
    color: "#f4d553",
    fontFamily: "OpenSans-Bold",
    fontSize: "14px",
    cursor: "pointer",
  },
  welcomtxt: {
    color: "#25c9d4",
    fontFamily: "OpenSans-Extrabold",
    fontSize: "24px",


  },
  subtit: {
    color: "#eeeeee",
    fontFamily: "OpenSans-Regular",
    fontSize: "22px",
  }

};

export class Forgot_password extends Component {
  constructor(props) {
    super(props)

    this.state = {

      open: false,
      errorMsg: '',
      password1: '',
      password2: ''

    }
  }
  render() {
    const { password1, open, errorMsg, password2 } = this.state
    return (
      <div>
        <Grid container style={styles.paperContainer}>

          <Grid item xs={6}
          >


            <Grid direction="row" item xs={8} style={{ marginTop: '198px', marginLeft: '149px' }}>

              <img
                src={mylogo}
                alt="loginb"
                height='160px'
                width='198px'


              />
              <Grid style={{ paddingTop: '20px' }}><Typography style={styles.welcomtxt}>
                Welcome to CFD Trading</Typography>
              </Grid>
              <Grid>
                <Typography style={styles.subtit}>This is an Artificial Intelligence-based
                  prediction engine applicable for
                  demonstrated by machines, unlike</Typography>
              </Grid>
            </Grid>



          </Grid>
          <Grid item xs={6}>
            <Grid container item xs={12} direction="row" style={styles.sigingrid}>

              <Grid style={styles.contpadd}>
                <Button



                  variant="text"
                  size="large"
                  style={{
                    color: "#f4d553",
                    fontFamily: "OpenSans-Bold",
                    borderRadius: "4px",


                    fontSize: "14px",
                    lineHeight: "19px",
                    textTransform: "none",



                  }}
                  startIcon={<ArrowBack />}
                  onClick={() => {
                    this.props.history.push("/login", this.state);
                  }}
                >

                  BACK
                </Button>
                <Typography style={styles.loginhead}>Reset password</Typography>

                <Grid style={{ margin: '8px 0px 0px 0px' }}><Typography style={styles.loginsubhead}>OTP</Typography></Grid>

                <Grid style={{ margin: '5px 0px 0px 0px' }}>
                  <OtpInput

                    numInputs={4}
                    isInputNum={true}
                    isInputSecure={true}
                    separator={<span>-</span>}
                    inputStyle={{
                      backgroundColor: "white !important",
                      width: "45px",
                      height: "46px",

                      borderRadius: "8px",
                      border: "none",
                      marginRight: "5px",
                      color: "#2f3253",
                      fontSize: "16px",
                      fontFamily: "OpenSans-Regular"


                    }}
                    containerStyle={{
                      justifyContent: "center",
                      border: '0px',

                    }}
                  />
                </Grid>



                <Grid style={{ margin: '10px 0px 0px 0px' }}>
                  <CustomTextField
                    autoFocus={true}
                    style={styles.field}
                    value={password1}
                    placeholder="New Password"
                    fullWidth={true}
                    type="password"
                    hasEndAdornment={true}
                    onChange={this.handleChange}
                  />
                </Grid>

                <Grid style={{ margin: '15px 0px 0px 0px' }}>
                  <CustomTextField
                    autoFocus={true}
                    style={styles.field}
                    value={password2}
                    placeholder="Confirm password"
                    fullWidth={true}
                    type="password"
                    hasEndAdornment={true}
                    onChange={this.handleChange}
                  />
                </Grid>

                <Grid style={{ margin: '20px 0px 0px 0px' }}>
                  <Button
                    fullWidth


                    variant="contained"
                    size="large"
                    style={{
                      color: "#2f3253",
                      fontFamily: "OpenSans-Bold",
                      borderRadius: "4px",
                      backgroundColor: "#f4d553",

                      fontSize: "14px",
                      lineHeight: "19px",
                      textTransform: "none",
                      padding: "12px 16px",
                      marginBottom: "5%"

                    }}
                    onClick={() => {
                      this.props.history.push("/login", this.state);
                    }}
                  >

                    SAVE
                  </Button>
                </Grid>

              </Grid>


            </Grid>

          </Grid>
        </Grid>


        <Snackbar

          anchorOrigin={{
            horizontal: "right",
            vertical: "bottom",
          }}
          open={open}
          autoHideDuration={50}
          message={errorMsg}

          action={
            <React.Fragment>

              <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={() => { this.handleToClose() }}
              >
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
      </div>
    )
  }
}

export default Forgot_password
