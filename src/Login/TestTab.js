import React, { useState } from 'react'
import { Grid, Box } from '@material-ui/core'
import { BrowserRouter as Router, Switch, Route, useHistory } from 'react-router-dom';


export default function TestTab() {



  const styles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
    }
  };


  function Page1() {
    return (
      <h1>a</h1>
    )
  }

  function b() {
    return (
      <h1>b</h1>
    )
  }


  function c() {
    return (
      <h1>c</h1>
    )
  }

  const [myst, setMyst] = useState('a111');
  const [myst1, setMyst1] = useState(styles.mylineborder);
  const [myst2, setMyst2] = useState(styles.mylineborder);



  const history = useHistory();

  const handleClickToOpen = () => {
    setMyst('a1');

  };
  const handleClickToOpen1 = () => {
    setMyst('b');


  };

  const handleClickToOpen2 = () => {
    setMyst2(styles.mylinefill);
    setMyst(styles.mylineborder);
    setMyst1(styles.mylineborder);

  };




  return (

    <div>


      <h1>abc</h1>

      <button onClick={handleClickToOpen}>1</button>
      <button onClick={handleClickToOpen1}>2</button>
      <div id="content-wrapper">
        {/* only render the component that matches the state */}
        {/* this could easily be a switch() statement as well */}

        {myst === 'a1' && <h1>Page1</h1>}
        {myst === 'b' && <h1>Page2</h1>}
      </div>
    </div>

  )
}

