import React, { Component } from "react";
import { Grid, Typography, Button, IconButton, Snackbar } from "@material-ui/core"
import bg from '../ui-assets/images/register-bg.jpeg'
import signupimg from '../ui-assets/images/CFD_bg.png'
import mylogo from '../ui-assets/images/logo.svg'
import emailimg from '../ui-assets/images/email.svg'
import CloseIcon from '@material-ui/icons/Close'
import OtpInput from "react-otp-input";

const styles = {
  paperContainer: {
    height: 'auto',
    backgroundImage: `url(${bg})`,

    backgroundPosition: 'center',
    margin: '0px 0px',
    backgroundSize: "100% 100%",
  },
  loadlogo: {

    backgroundImage: `url(${mylogo})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: '100%',
  },
  sigingrid: {
    width: '482px',
    height: '442px',
    margin: '162px 13px 42px 48px',
    backgroundSize: 'cover',
    backgroundImage: `url(${signupimg})`,
  },
  contpadd: {
    marginLeft: '92px',
    marginTop: '55px',

  },

  loginhead: {
    color: "#ffffff",
    fontFamily: " OpenSans-Extrabold",
    fontSize: "22px",

  },
  loginsubhead: {
    color: "#dddddd",
    fontFamily: "OpenSans-Regular",
    fontSize: "16px",
  },
  otpStyles: {
    backgroundColor: "white !important",
    width: "45px !important",
    height: "46px !important",
    borderRadius: "8px",
    border: "none",
    marginRight: "5px",
    color: "#2f3253",
    fontSize: "16px",
    fontFamily: "OpenSans-Regular"
  },
  field: {
    marginLeft: "2px",
    marginRight: "2px",
    height: '45px',

  },
  loginwithpass: {
    color: "#f4d553",
    fontFamily: "OpenSans-Bold",
    fontSize: "14px",
    cursor: "pointer",

  },
  haveaccount: {
    color: "#ffffff",
    fontFamily: "OpenSans",
    fontSize: "16px",
  },
  backbutton: {
    color: "#f4d553",
    fontFamily: " OpenSans-Regular",
    fontSize: "14px",
    cursor: "pointer",
  },
  welcomtxt: {
    color: "#25c9d4",
    fontFamily: "OpenSans-Extrabold",
    fontSize: "24px",


  },
  subtit: {
    color: "#eeeeee",
    fontFamily: "OpenSans-Regular",
    fontSize: "22px",
  }, redStyle: {
    color: "rgb(37, 39, 62) !important"
  },

};


export class LoginLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.location.state,
      emailid: this.props.location.state.emailid,
      open: false,
      errorMsg: ''
    }

  }
  GoToDashBord() {

    this.props.history.push("/dashbord", this.state);


  }

  render() {

    const { emailid, open, errorMsg } = this.state;

    return (

      <div >

        <Grid container style={styles.paperContainer}>

          <Grid item xs={6}
          >


            <Grid direction="row" item xs={8} style={{ marginTop: '198px', marginLeft: '149px' }}>

              <img
                src={mylogo}
                alt="loginb"
                height='160px'
                width='198px'


              />
              <Grid style={{ paddingTop: '20px' }}><Typography style={styles.welcomtxt}>
                Welcome to CFD Trading</Typography>
              </Grid>
              <Grid>
                <Typography style={styles.subtit}>This is an Artificial Intelligence-based
                  prediction engine applicable for
                  demonstrated by machines, unlike</Typography>
              </Grid>
            </Grid>



          </Grid>
          <Grid item xs={6}>
            <Grid container item xs={12} direction="row" style={styles.sigingrid}>
              <Grid item xs={8} style={styles.contpadd}>
                <Typography style={styles.backbutton}

                  onClick={() =>
                    this.props.history.push("/")
                  }
                >&lt; BACK</Typography>



                <img
                  src={emailimg}
                  alt="loginb"
                  height='56px'
                  width='56px'


                />

                <Typography style={styles.loginhead}>
                  JUST ONE MORE STEP,
                </Typography>
                <Typography style={styles.loginhead}>
                  LET’S VERIFY YOUR EMAIL
                </Typography>
                <Typography style={styles.loginsubhead}>
                  Verify your email with OTP sent to
                </Typography>
                <Typography style={styles.loginsubhead}>
                  {emailid}
                </Typography>

                <OtpInput

                  numInputs={4}
                  isInputNum={true}
                  isInputSecure={true}
                  separator={<span>-</span>}
                  inputStyle={{
                    backgroundColor: "white !important",
                    width: "45px",
                    height: "46px",

                    borderRadius: "8px",
                    border: "none",
                    marginRight: "5px",
                    color: "#2f3253",
                    fontSize: "16px",
                    fontFamily: "OpenSans-Regular"


                  }}
                  containerStyle={{
                    justifyContent: "center",
                    border: '0px',

                  }}
                />
                <Grid style={{ paddingTop: '25px' }}>
                  <Button
                    fullWidth


                    variant="contained"
                    size="large"
                    style={{
                      color: "#2f3253",
                      fontFamily: "OpenSans-Bold",
                      borderRadius: "4px",
                      backgroundColor: "#f4d553",

                      fontSize: "14px",
                      lineHeight: "19px",
                      textTransform: "none",
                      padding: "12px 16px",
                      marginBottom: "5%"

                    }}
                    onClick={() => { this.GoToDashBord() }}
                  >

                    VERIFY AND LOGIN
                  </Button>
                </Grid>
              </Grid>
            </Grid>

          </Grid>
        </Grid>
        <Snackbar

          anchorOrigin={{
            horizontal: "right",
            vertical: "bottom",
          }}
          open={open}
          autoHideDuration={50}
          message={errorMsg}

          action={
            <React.Fragment>

              <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={() => { this.handleToClose() }}
              >
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
      </div>
    );
  }
}
export default LoginLayout;