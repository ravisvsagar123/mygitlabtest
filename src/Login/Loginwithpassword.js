import React, { Component } from "react";
import { Grid, Typography, Button, Snackbar, IconButton } from "@material-ui/core"
import bg from '../ui-assets/images/register-bg.jpeg'
import signupimg from '../ui-assets/images/CFD_bg.png'
import CustomTextField from "../ui-atoms/components/CustomTextField"
import mylogo from '../ui-assets/images/logo.svg'
import CloseIcon from '@material-ui/icons/Close'
import validator from 'validator'


const styles = {
  paperContainer: {
    height: '100vh',
    backgroundImage: `url(${bg})`,

    backgroundPosition: 'center',
    margin: '0px 0px',
    backgroundSize: 'cover',
  },
  loadlogo: {

    backgroundImage: `url(${mylogo})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: '100%',
  },
  sigingrid: {
    width: '482px',
    height: '442px',
    backgroundSize: 'cover',
    backgroundImage: `url(${signupimg})`,
    marginTop: '15%'

  },
  contpadd: {
    marginLeft: '92px',
    marginTop: '50px',

  },

  loginhead: {
    color: "#fff",
    fontFamily: " OpenSans-Extrabold",
    fontSize: "22px",
    paddingTop: '10PX',
  },
  loginsubhead: {
    color: "#dddddd",
    fontFamily: "OpenSans-Regular",
    fontSize: "16px",
    paddingTop: '8PX',
  },
  otpStyles: {
    backgroundColor: "white !important",
    width: "45px !important",
    height: "46px !important",
    borderRadius: "8px",
    border: "none",
    marginRight: "5px",
    color: "#2f3253",
    fontSize: "16px",
    fontFamily: "OpenSans-Regular"
  },
  field: {
    marginLeft: "2px",
    marginRight: "2px",
    height: '45px',

  },
  loginwithpass: {
    color: "#f4d553",
    fontFamily: "OpenSans-Bold",
    fontSize: "14px",
    cursor: "pointer",

  },
  haveaccount: {
    color: "#f4d553",
    fontFamily: "OpenSans-Bold",
    fontSize: "14px",
    cursor: "pointer",

  },
  haveaccount1: {
    color: "#ffffff",
    fontFamily: "OpenSans-Regular",
    fontSize: "16px",

  },
  signuptxt: {
    color: "#f4d553",
    fontFamily: "OpenSans-Bold",
    fontSize: "14px",
    cursor: "pointer",

  },

  backbutton: {
    color: "#f4d553",
    fontFamily: "OpenSans-Regular",
    fontSize: "14px",
    cursor: "pointer",
  },
  welcomtxt: {
    color: "#25c9d4",
    fontFamily: "OpenSans-Extrabold",
    fontSize: "24px",


  },
  subtit: {
    color: "#eeeeee",
    fontFamily: "OpenSans-Regular",
    fontSize: "22px",
  }, redStyle: {
    color: "rgb(37, 39, 62) !important"
  },
  mymodel: {
    width: '840px',
    height: '357px',
    background: '#fff',
    margin: '206px 246px 83px 140px',
    borderRadius: '4px',
    border: '2px solid #000',
  }
};


export class Loginwithpassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: this.props.location.state,
      emailid: this.props.location.state.emailid,
      password: '',
      open: false,
      errorMsg: ''
    }

  }
  handleChange = (e) => this.setState({
    password: e.target.value
  })
  handleChangeemail = (e) => this.setState({
    emailid: e.target.value
  })
  handleToClose() {

    this.setState({ open: false })



  }
  GoToDashBord() {
    var password = this.state.password;
    var emailid = this.state.emailid;
    var isok = '0';
    if (!emailid.length) {
      isok = '1';

      this.setState({ open: true, errorMsg: 'Enter the Email ID' })



    }
    if (!validator.isEmail(emailid) && isok === '0') {
      isok = 1;
      this.setState({ open: true, errorMsg: 'Enter valid Email!' })

    }
    if (!password.length && isok === '0') {
      isok = 1;

      this.setState({ open: true, errorMsg: 'Enter the Password' })



    }
    if (isok === '0') {
      this.props.history.push("/dashbord", this.state);

    }
  }

  handleOTP() {

    var emailid = this.state.emailid;

    var isok = '0';
    if (!emailid.length) {
      isok = '1';

      this.setState({ open: true, errorMsg: 'Enter the Email ID' })



    }
    if (!validator.isEmail(emailid) && isok === '0') {
      isok = 1;
      this.setState({ open: true, errorMsg: 'Enter valid Email!' })

    }
    if (isok === '0') {

      this.props.history.push("/otp", this.state);
    }
  }
  handleFP() {
    var emailid = this.state.emailid;

    var isok = '0';
    if (!emailid.length) {
      isok = '1';

      this.setState({ open: true, errorMsg: 'Enter the Email ID' })



    }
    if (isok === '0') {
      this.props.history.push("/fp", this.state);

    }
  }

  render() {
    const { emailid, password, open, errorMsg } = this.state;
    return (
      <div >

        <Grid container style={styles.paperContainer}>

          <Grid item xs={6}
          >


            <Grid direction="row" item xs={8} style={{ marginTop: '198px', marginLeft: '149px' }}>

              <img
                src={mylogo}
                alt="loginb"
                height='160px'
                width='198px'


              />
              <Grid style={{ paddingTop: '20px' }}><Typography style={styles.welcomtxt}>
                Welcome to CFD Trading</Typography>
              </Grid>
              <Grid>
                <Typography style={styles.subtit}>This is an Artificial Intelligence-based
                  prediction engine applicable for
                  demonstrated by machines, unlike</Typography>
              </Grid>
            </Grid>



          </Grid>



          <Grid item xs={6}>
            <Grid container item xs={12} style={styles.sigingrid}>
              <Grid item xs={8} style={styles.contpadd}>
                <Typography style={styles.backbutton}

                  onClick={() =>
                    this.props.history.push("/")
                  }
                >&lt; BACK</Typography>




                <Typography style={styles.loginhead}>
                  Login with CFD
                </Typography>

                <Typography style={styles.loginsubhead}>
                  Enter your email id
                </Typography>
                <Grid style={{ paddingTop: '10px' }}>
                  <CustomTextField
                    autoFocus={true}
                    style={styles.field}
                    value={emailid}
                    placeholder="Email address"
                    fullWidth={true}
                    onChange={this.handleChangeemail}
                  />
                </Grid>
                <Grid style={{ paddingTop: '18px' }}>
                  <CustomTextField
                    autoFocus={true}
                    style={styles.field}
                    value={password}
                    placeholder="Password"
                    fullWidth={true}
                    type="password"
                    hasEndAdornment={true}
                    onChange={this.handleChange}
                  />
                </Grid>
                <Grid container style={{ paddingTop: '6px' }}>
                  <Grid item xs={6}> <Typography style={styles.haveaccount}
                    onClick={() =>
                      this.handleOTP()

                    }>
                    Login with OTP
                  </Typography></Grid>
                  <Grid item xs={6} style={{ textAlign: 'right' }}>
                    <Typography style={styles.haveaccount}
                      onClick={() => {
                        this.handleFP()
                      }}>

                      Forgot password?
                    </Typography></Grid>
                </Grid>
                <Grid style={{ paddingTop: '20px' }}>
                  <Button
                    fullWidth

                    backgroundColor="transparent"
                    variant="contained"
                    size="large"
                    style={{
                      color: "#2f3253",
                      fontFamily: "OpenSans-Bold",
                      borderRadius: "4px",
                      backgroundColor: "#f4d553",

                      fontSize: "14px",
                      lineHeight: "19px",
                      textTransform: "none",
                      padding: "12px 16px",


                    }}

                    onClick={() => { this.GoToDashBord() }}

                  >

                    LOGIN
                  </Button>
                </Grid>
                <Grid container style={{ paddingTop: '10px' }}>
                  <Grid item xs={8}> <Typography style={styles.haveaccount1}>
                    Don't have an account
                  </Typography></Grid>
                  <Grid item xs={4}><Typography style={styles.signuptxt} onClick={() =>
                    this.props.history.push("/signup", this.state)
                  }>
                    SIGNUP
                  </Typography></Grid>
                </Grid>
              </Grid>

            </Grid>

          </Grid>
        </Grid>
        <Snackbar

          anchorOrigin={{
            horizontal: "right",
            vertical: "bottom",
          }}
          open={open}
          autoHideDuration={50}
          message={errorMsg}

          action={
            <React.Fragment>

              <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={() => { this.handleToClose() }}
              >
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />

      </div>
    );
  }
}
export default Loginwithpassword;