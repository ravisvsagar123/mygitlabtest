import React, { Component } from "react";
import { Grid, Typography, Button, IconButton, Snackbar } from "@material-ui/core"
import bg from '../ui-assets/images/register-bg.jpeg'
import CustomTextField from "../ui-atoms/components/CustomTextField"
import mylogo from '../ui-assets/images/logo.svg'

import emailimg from '../ui-assets/images/email.svg'
import signupl from '../ui-assets/images/signupl.svg'
import signupr from '../ui-assets/images/signupr.svg'
import signupimg from '../ui-assets/images/CFD_bg.png'

import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import CloseIcon from "@material-ui/icons/Close";

import validator from 'validator'
const styles = {
    paperContainer: {
        height: '100vh',
        backgroundImage: `url(${bg})`,

        backgroundPosition: 'center',
        margin: '0px 0px',
        backgroundSize: 'cover',
    },
    loadlogo: {

        backgroundImage: `url(${mylogo})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        height: '100%',
    },
    modelhead: {
        color: "#454871",
        fontFamily: "OpenSans-Extrabold",
        fontSize: "22px",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    infosty: {
        color: "#454871",
        fontFamily: "OpenSans-Semibold",
        fontSize: "12px",
        paddingTop: '15px'
    },
    sigingrid: {
        width: '482px',
        height: '442px',
        backgroundSize: 'cover',
        backgroundImage: `url(${signupimg})`,
        marginTop: '15%'
    },
    contpadd: {
        margin: '47px 100px 10px 92px',

        width: '400px',
        height: '220px',
    },
    contpadd1: {
        margin: '230px 149px 10px 92px',

        width: '400px',
        height: '220px',
    },
    loginhead: {
        color: "#fff",
        fontFamily: " OpenSans-Extrabold",
        fontSize: "22px",

    },
    loginsubhead: {
        color: "#dddddd",
        fontFamily: "OpenSans-Regular",
        fontSize: "16px",
    },
    field: {
        marginLeft: "2px",
        marginRight: "2px",
        height: '45px',

    },
    loginwithpass: {
        color: "#f4d553",
        fontFamily: "OpenSans-Bold",
        fontSize: "14px",
        cursor: "pointer",

    },
    haveaccount: {
        color: "#ffffff",
        fontFamily: "OpenSans",
        fontSize: "16px",
    },
    signup: {
        color: "#f4d553",
        fontFamily: "OpenSans-Bold",
        fontSize: "14px",
        cursor: "pointer",
    },
    welcomtxt: {
        color: "#25c9d4",
        fontFamily: "OpenSans-Extrabold",
        fontSize: "24px",


    },
    subtit: {
        color: "#eeeeee",
        fontFamily: "OpenSans-Regular",
        fontSize: "22px",
    },
    gsty: {
        borderRadius: '12px',


        height: '215px',
        width: '348px',

        border: '#dddddd 2px solid',

    }, closeButton: {
        position: "absolute",
        right: "33px",
        top: "7px",
        color: "#454871"
    },

};


export class Signup extends Component {
    constructor(props) {
        super(props)

        this.state = {
            value: this.props.location.state,
            emailid: this.props.location.state.emailid,
            open: false,
            openDialog: false,
            errorMsg: ''

        }
    }


    handleClickToOpen() {
        var emailid = this.state.emailid;

        var isok = '0';

        if (!emailid.length) {
            isok = 1;

            this.setState({ open: true, errorMsg: 'Email ID Rerequired' })



        }
        if (!validator.isEmail(emailid) && isok === '0') {
            isok = 1;
            this.setState({ open: true, errorMsg: 'Enter valid Email!' })

        }

        if (isok === '0') {
            this.setState({ openDialog: true })

        }
    }


    closeContactModal() {
        this.setState({ openDialog: false })
    }
    handleChange = (e) => this.setState({
        emailid: e.target.value
    })
    render() {
        const { open, emailid, openDialog, errorMsg } = this.state
        return (
            <div >

                <Grid container style={styles.paperContainer}>

                    <Grid item xs={6}
                    >


                        <Grid direction="row" item xs={8} style={{ marginTop: '198px', marginLeft: '149px' }}>

                            <img
                                src={mylogo}
                                alt="loginb"
                                height='160px'
                                width='198px'


                            />
                            <Grid style={{ paddingTop: '20px' }}><Typography style={styles.welcomtxt}>
                                Welcome to CFD Trading</Typography>
                            </Grid>
                            <Grid>
                                <Typography style={styles.subtit}>This is an Artificial Intelligence-based
                                    prediction engine applicable for
                                    demonstrated by machines, unlike</Typography>
                            </Grid>
                        </Grid>



                    </Grid>
                    <Grid item xs={6}>
                        <Grid container item xs={12} direction="row" style={styles.sigingrid}>
                            <Grid item xs={8} style={styles.contpadd}>



                                <img
                                    src={emailimg}
                                    alt="loginb"
                                    height='56px'
                                    width='56px'


                                />

                                <Typography style={styles.loginhead}>
                                    Get’s Started

                                </Typography>
                                <Typography style={styles.loginhead}>
                                    Signup with your email
                                </Typography>

                                <Typography style={styles.loginsubhead}>
                                    Once you verified, you no need to do this anymore:)
                                </Typography>
                                <Grid style={{ paddingTop: '25px' }}>
                                    <CustomTextField
                                        autoFocus={true}
                                        style={styles.field}
                                        value={emailid}
                                        placeholder="Email address"
                                        fullWidth={true}
                                        onChange={this.handleChange}
                                    />
                                </Grid>
                                <Grid style={{ paddingTop: '25px' }}>
                                    <Button
                                        fullWidth

                                        backgroundColor="transparent"
                                        variant="contained"
                                        size="large"
                                        style={{
                                            color: "#2f3253",
                                            fontFamily: "OpenSans-Bold",
                                            borderRadius: "4px",
                                            backgroundColor: "#f4d553",

                                            fontSize: "14px",
                                            lineHeight: "19px",
                                            textTransform: "none",
                                            padding: "12px 16px",
                                            marginBottom: "5%"

                                        }}
                                        onClick={() => { this.handleClickToOpen() }}
                                    >

                                        NEXT
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>

                    </Grid>
                    <Dialog
                        onClose={() => this.closeContactModal()}
                        open={openDialog}
                        maxWidth="md"
                        fullWidth
                        BackdropProps={{
                            style: {
                                backgroundColor: "#rgba(0, 0, 0, 0.16)",
                                backdropFilter: "blur(30px)"
                            }
                        }}
                        PaperProps={{
                            style: {
                                background: "#fff",
                                borderRadius: 6,


                            }
                        }}
                    >
                        <DialogTitle>

                            <Grid container>
                                <Grid xs={11} >
                                    <Typography

                                        style={styles.modelhead}
                                    >
                                        LET’S START TRADE IN YOUR ACOUNT
                                    </Typography>

                                </Grid>
                                <Grid xs={1}> <IconButton
                                    aria-label="close"
                                    style={styles.closeButton}
                                    onClick={() => this.closeContactModal()}
                                >
                                    <CloseIcon />
                                </IconButton></Grid>
                            </Grid>
                        </DialogTitle>
                        <DialogContent style={{ height: '260px', paddingLeft: '48px', display: "flex" }}>

                            <Grid container style={{ paddingLeft: '48px' }}>
                                <Grid container item style={styles.gsty} xs={5} display='flex' >
                                    <Grid item xs={2} style={{ paddingTop: '60px', justifyItems: 'center' }}>
                                        <img
                                            src={signupl}
                                            alt="loginb"
                                            height='75px'
                                            width='66px'


                                        />
                                    </Grid>
                                    <Grid item xs={10} style={{ paddingTop: '40px' }}>
                                        <li style={styles.infosty}>Premim trading experince</li>
                                        <li style={styles.infosty}>Lorem Ipsum is simply dummy</li>
                                        <li style={styles.infosty}>Lorem Ipsum is simply</li>
                                    </Grid>
                                    <Grid item xs={12} style={{
                                        display: 'flex',
                                        alignItems: 'center',

                                        justifyContent: 'center', paddingTop: '5px',

                                    }}>
                                        <Button
                                            fullWidth

                                            backgroundColor="transparent"
                                            variant="contained"
                                            size="large"
                                            style={{
                                                color: "#2f3253",
                                                fontFamily: "OpenSans-Extrabold",
                                                borderRadius: "4px",
                                                backgroundColor: "#f4d553",
                                                width: '300px',
                                                fontSize: "14px",
                                                lineHeight: "19px",
                                                textTransform: "none",
                                                padding: "12px 16px",


                                            }}
                                            onClick={() => this.props.history.push('/OpenRealAccount')}
                                        >

                                            OPEN REAL ACCOUNT
                                        </Button>

                                    </Grid>

                                </Grid>

                                <Grid container item xs={1}></Grid>

                                <Grid container item style={styles.gsty} xs={5} display='flex' >
                                    <Grid item xs={2} style={{ paddingTop: '60px', justifyItems: 'center' }}>
                                        <img
                                            src={signupr}
                                            alt="loginb"
                                            height='75px'
                                            width='66px'


                                        />
                                    </Grid>
                                    <Grid item xs={10} style={{ paddingTop: '40px' }}>
                                        <li style={styles.infosty}>Premim trading experince</li>
                                        <li style={styles.infosty}>Lorem Ipsum is simply dummy</li>
                                        <li style={styles.infosty}>Lorem Ipsum is simply</li>
                                    </Grid>
                                    <Grid item xs={12} style={{
                                        display: 'flex',
                                        alignItems: 'center',

                                        justifyContent: 'center', paddingTop: '5px',

                                    }}>
                                        <Button
                                            fullWidth

                                            backgroundColor="transparent"
                                            variant="contained"
                                            size="large"
                                            style={{
                                                color: "#2f3253",
                                                fontFamily: "OpenSans-Extrabold",
                                                borderRadius: "4px",
                                                backgroundColor: "#f4d553",
                                                width: '300px',
                                                fontSize: "14px",
                                                lineHeight: "19px",
                                                textTransform: "none",
                                                padding: "12px 16px",


                                            }}
                                            onClick={() => this.props.history.push('/opendemo')}
                                        >

                                            OPEN DEMO ACCOUNT
                                        </Button>

                                    </Grid>

                                </Grid>

                            </Grid>


                        </DialogContent>
                    </Dialog>
                </Grid>
                <Snackbar

                    anchorOrigin={{
                        horizontal: "right",
                        vertical: "bottom",
                    }}
                    open={open}
                    autoHideDuration={50}
                    message={errorMsg}

                    action={
                        <React.Fragment>

                            <IconButton
                                size="small"
                                aria-label="close"
                                color="inherit"
                                onClick={() => { this.setState({ open: false }) }}
                            >
                                <CloseIcon fontSize="small" />
                            </IconButton>
                        </React.Fragment>
                    }
                />
            </div >
        );
    }
}
export default Signup;