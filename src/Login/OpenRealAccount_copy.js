import React, { Component } from 'react'
import {
  Grid, Typography, Button, Box, makeStyles,
  Step, Stepper, StepLabel, Checkbox

} from '@material-ui/core'
import accbg from '../ui-assets/images/Rectangle.svg'
import ArrowBack from "@material-ui/icons/KeyboardArrowLeft"

import CustomTextField from "../ui-atoms/components/CustomTextField"
import Select from "react-select";
import countries from "../ui-config/countries.json"
import cfdimg from '../ui-assets/images/cfd.png'
import empstatus from '../ui-assets/images/empstatus.png'
import cfdpage3 from '../ui-assets/images/cfdpage3.png'

import uploadimg from '../ui-assets/images/upload.svg'
import selectedfile from '../ui-assets/images/selectedfile.svg'
import QRCode from 'qrcode.react';
//import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';

import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';

const styles = {
  mainback: {



    height: '100vh',



    background: 'linear-gradient(119deg, #00cfd2, #006aab 91%, #0060a7)',
  },
  paperContainer: {
    height: '768px',
    width: 'auto',
    backgroundImage: `url(${accbg})`,

    backgroundPosition: 'center',
    margin: '0px 0px',
    backgroundSize: "cover",
  },
  mainbacksub: {
    width: '830px',
    height: '623px',
    background: '#fff',
    borderRadius: '12px',
    justifyContent: 'center',

    margin: '10px 75px 1px 268px'
  },
  mainheadersub: {
    color: "#ffffff",
    fontFamily: "OpenSans-Regular",
    fontSize: "18px",

  },
  demohead: {
    color: "#333333",
    fontFamily: "OpenSans-Regular",
    fontSize: "18px",

  },
  line: {
    border: '#dddddd 1px solid',
  },
  field: {
    marginLeft: "2px",
    marginRight: "2px",
    height: '34px',
    width: '300px',

  },
  mainheader: {
    color: "#ffffff",
    fontFamily: "OpenSans-Bold",
    fontSize: "24px",
  },
  labelContainer: {
    "& $alternativeLabel": {
      marginTop: 0
    }
  },

};
export class PersonalInfo extends Component {

  constructor(props) {
    super(props)

    this.state = {

      isfname: false

    }
  }

  abc() {

    alert("ravi sagar");
  }

  handleInputChange(e) {
    this.props.onNameChange(e.target.value)
  }

  render() {

    const select_style = {
      control: base => ({
        ...base,
        border: 0,

        boxShadow: "none",
        fontSize: '14px',
        fontFamily: 'OpenSans-Regular',
        colour: '#999999'
      })
    };
    const countriesData = countries.map(data => {
      return { label: data.country, value: data.countryISO2 };
    });

    const acctype = [
      { value: 'STANDARD', label: 'STANDARD' },
      { value: 'ELITE', label: 'ELITE' },
      { value: 'PREMIUM', label: 'PREMIUM' },
    ]

    const cltype = [
      { value: 'Individual_Account', label: 'Individual Account' },
      { value: 'Common_Account', label: 'Common Account' },
      { value: 'Corporate_Account', label: ' Corporate Account' },
    ]

    const currtype = [
      { value: 'EUR', label: 'EUR' },
      { value: 'USD', label: 'USD' },

    ]

    const { fname } = this.state;
    return (

      < div >
        <Grid style={styles.line}></Grid>
        <Grid container style={{ height: '95%' }}>
          <Grid item xs={1}></Grid>

          <Grid item xs={4}>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="First Name*"
                value={fname}
                handleChange={e =>
                  this.handleInputChange(e)

                }
              />

            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Last Name*"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Email*"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Mobile No*"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Address"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Postal code"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px', width: '300px', height: '45px' }}>
              <Select styles={select_style} options={countriesData} style={{ width: '300px', height: '45px' }} placeholder="Country" />
            </Grid>
          </Grid>

          <Grid item xs={1}></Grid>
          <Grid item xs={4}>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="State"
              />

            </Grid>
            <Grid style={{ paddingTop: '10px' }} >

              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="City"
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select styles={select_style} options={currtype} style={{ width: '300px', height: '45px' }} placeholder="Currency choice" />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select styles={select_style} options={acctype} style={{ width: '300px', height: '45px' }} placeholder="Account Type" />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select styles={select_style} options={cltype} style={{ width: '300px', height: '45px' }} placeholder=" Client Type" />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Password*"
                fullWidth={true}
                type="Password"
                hasEndAdornment={true}
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Password Confirmation*"
                fullWidth={true}
                type="Password*"
                hasEndAdornment={true}
              />
            </Grid>

          </Grid>





        </Grid>
      </div >
    )


  }
}
function AccountType(params) {

  return (
    <div>
      <h1>2</h1>
    </div>
  )
}
function Documents(params) {

  return (
    <div>
      <h1>3</h1>
    </div>
  )
}

function IndustryExp(params) {

  return (
    <div>
      <h1>4</h1>
    </div>
  )
}
export class OpenRealAccount_copy extends Component {
  constructor(props) {
    super(props)

    this.state = {
      activeStep: 0,

      open: false,
      openDialog: false,
      errorMsg: '',
      fname: '',
      isfname: false
    }
  }



  handleNext() {
    var activeStep = this.state.activeStep;
    var fname = this.state.fname;
    alert(fname);
    this.setState({ isfname: true })
    /*if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }
  */


    if (activeStep === 0) {
      alert(activeStep);
    }
    this.setState({ activeStep: activeStep + 1 });
    //setSkipped(newSkipped);
  };

  handleNameChange = fname => {
    this.setState({ fname })
  }
  getStepContent(step) {
    switch (step) {
      case 0:
        return <PersonalInfo onNameChange={this.handleNameChange} />;
      case 1:
        return <AccountType />;
      case 2:
        return <Documents />;
      case 3:
        return <IndustryExp />;

      default:
        return "Unknown step";
    }


  }
  render() {
    const c = makeStyles(() => ({
      root: {
        "& .MuiStepIcon-active": { color: "#f4d553" },
        "& .MuiStepIcon-completed": { color: "#48d74d" },
        "& .Mui-disabled .MuiStepIcon-root": { color: "#ffffff" },
        "& .Mui-disabled .MuiStepLabel-iconContainer": { color: "red" },


      },
      text: {
        color: '#D3D3D3',
      },
    }));
    const { activeStep } = this.state
    const steps = ['Personal Info', ' Account Type', 'Documents', 'Industry Exp'];

    return (
      <div>
        <Grid style={styles.paperContainer}>
          <Grid style={{ justifyContent: 'center', display: 'flex' }}>
            <Typography style={styles.mainheader}>Registration. Use form below</Typography>
          </Grid>
          <Grid style={{ justifyContent: 'center', display: 'flex' }}>
            <Typography style={styles.mainheadersub}>Please fill in your details to create a trading account.</Typography>
          </Grid>

          <Grid direction='row' style={styles.mainbacksub}>
            <Grid style={{ height: '11%', marginRight: '2%', marginLeft: '2%', alignSelf: 'start' }}>
              <Stepper className={c.root} activeStep={activeStep} style={{ paddingTop: '15px', borderRadius: '12px' }}>
                {steps.map((label, index) => {
                  const stepProps = {};
                  const labelProps = {};


                  return (
                    <Step key={label} {...stepProps}>
                      <StepLabel  {...labelProps}>{label}</StepLabel>
                    </Step>
                  );
                })}
              </Stepper>
            </Grid>
            <Grid style={{ height: '78%', alignSelf: 'center' }}>
              {this.getStepContent(activeStep)}
            </Grid>
            <Grid style={{ display: 'flex', height: '11%', marginRight: '7%', marginLeft: '7%', alignSelf: 'end' }}>
              <Grid item xs={8} style={{ paddingTop: '1%' }}>
                <Button
                  backgroundColor="transparent"
                  variant="text"
                  style={{
                    color: "#2f3253",
                    fontFamily: "OpenSans-Bold",
                    borderRadius: "4px",

                    fontSize: "14px",
                    lineHeight: "19px",
                    textTransform: "none",
                    padding: "12px 16px",
                    marginBottom: "5%"

                  }}


                  onClick={() => { this.handleBack() }}
                  sx={{ mr: 1 }}
                  startIcon={<ArrowBack />}
                >BACK
                </Button></Grid>
              <Grid item xs={2} style={{ paddingTop: '1%' }}>
                <Button
                  backgroundColor="transparent"
                  variant="outlined"
                  fullWidth
                  style={{
                    color: "#2f3253",
                    fontFamily: "OpenSans-Bold",
                    borderRadius: "4px",
                    border: '#454871 1px solid',
                    fontSize: "14px",
                    lineHeight: "19px",
                    textTransform: "none",
                    padding: "12px 16px",
                    marginBottom: "5%",
                    width: '82px'
                  }}>SAVE
                </Button>
              </Grid>
              <Grid item xs={2} style={{ paddingTop: '1%' }}>
                <Button
                  backgroundColor="transparent"
                  variant="contained"
                  fullWidth
                  style={{
                    color: "#2f3253",
                    fontFamily: "OpenSans-Bold",
                    borderRadius: "4px",
                    backgroundColor: "#f4d553",
                    fontSize: "14px",
                    lineHeight: "19px",
                    textTransform: "none",
                    padding: "12px 16px",
                    marginBottom: "5%"

                  }}
                  onClick={() => { this.handleNext() }}
                >NEXT
                </Button>
              </Grid>

            </Grid>

          </Grid>
        </Grid >
      </div>
    )
  }
}

export default OpenRealAccount_copy
