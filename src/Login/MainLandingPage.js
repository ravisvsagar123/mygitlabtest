import React from 'react'

import otp from './Loginwithotpotp';
import LoginLayout from './Login';
import Loginwithpassword from './Loginwithpassword';
import Signup from './Signup';
import OpenDemoAccount from './OpenDemoAccount';
import OpenRealAccount from './OpenRealAccount';
import dashbord from '../DashBord/MainDashBord';
import fp from './Forgot_password';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function MainLandingPage() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/" component={LoginLayout} />
          <Route path="/otp" component={otp} />
          <Route path="/login" component={Loginwithpassword} />
          <Route path="/signup" component={Signup} />
          <Route path="/opendemo" component={OpenDemoAccount} />
          <Route path="/OpenRealAccount" component={OpenRealAccount} />
          <Route path="/dashbord" component={dashbord} />
          <Route path="/fp" component={fp} />
        </Switch>
      </Router>
    </div>

  )
}

export default MainLandingPage
