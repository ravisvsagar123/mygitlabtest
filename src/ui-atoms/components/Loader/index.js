import React, { Fragment } from "react";
import Giff from "../../../ui-assets/images/bullforce-loader.gif";

const LoaderComp = () => {
  return (
    <Fragment>
      <img
        src={Giff}
        style={{ display: "block", margin: "auto" }}
        alt="loading..."
      />
    </Fragment>
  );
};

export default LoaderComp;
