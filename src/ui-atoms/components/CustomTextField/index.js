import React from "react";

//components
import {
  MuiThemeProvider,
  createMuiTheme,
  Paper,
  InputBase,
  withStyles,
  InputAdornment,
  IconButton
} from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

const styles = {
  paperRoot: {
    padding: "1px 1px",
    display: "flex",
    alignItems: "center",
    background: "#fff",
    borderRadius: "4px",
    border: "2px solid #dddddd"
  },
  paperRootError: {
    padding: "6px 10px",
    display: "flex",
    alignItems: "center",
    background: "#fff",
    border: "2px solid #FFB3B3"
  },
  input: {
    flex: 1,
    color: "#2f3253",
    fontSize: 16,
    fontFamily: "OpenSans",
    lineHeight: "10px",
    "& input[type=number]": {
      "-moz-appearance": "textfield"
    },
    "& input[type=number]::-webkit-outer-spin-button": {
      "-webkit-appearance": "none",
      margin: 0
    },
    "& input[type=number]::-webkit-inner-spin-button": {
      "-webkit-appearance": "none",
      margin: 0
    }
  }
};
const theme = createMuiTheme({
  overrides: {
    MuiPaper: {
      rounded: {
        borderRadius: 0
      },
      elevation1: {
        boxShadow: "none"
      }
    }
  }
});

class CustomTextField extends React.Component {
  state = {
    showPassword: false
  };
  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };
  render() {
    const {
      handleChange,
      fieldValue,
      placeholder,
      type,
      fullWidth,
      classes,
      isValid = true,
      hasEndAdornment = false,
      autoFocus = false,
      onCopyHandler,
      onCutHandler,
      onPasteHandler,
      ...rest
    } = this.props;
    const { showPassword } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <Paper className={isValid ? classes.paperRoot : classes.paperRootError}>
          <InputBase
            autoFocus={autoFocus}
            className={classes.input}
            type={
              type === "password"
                ? showPassword
                  ? "text"
                  : "password"
                : type === "number"
                ? "number"
                : type
            }
            value={fieldValue}
            onChange={handleChange}
            placeholder={placeholder}
            fullWidth={fullWidth}
            inputlabelprops={{ className: classes.input }}
            {...rest}
            endAdornment={
              hasEndAdornment && (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={this.handleClickShowPassword}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              )
            }
            inputProps={{
              // maxLength: 40,
              minLength: 0
            }}
            onCut={onCutHandler}
            onCopy={onCopyHandler}
            onPaste={onPasteHandler}
          />
        </Paper>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(CustomTextField);
